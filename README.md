# HouseDraft

If you want to run a small, [Swiss-style game tournament](https://en.wikipedia.org/wiki/Swiss-system_tournament), HouseDraft can help you with that. The name comes from the idea of running a *[Magic: The Gathering](https://magic.wizards.com/)* draft in your house, and the tiebreakers used are those recommended by [the DCI](https://www.wizards.com/default.asp?x=dci/welcome). But the site is "game-agnostic" and can be used to run tournaments for other formats or other games.

The benefit of a Swiss-style tournament is that players don't get eliminated. Every player can continue playing until the tournament is over, and will be ranked according to how well they do, and paired against opponents of similar rank (but never the same opponent twice). The downside (if you consider it one) is that there may not be a single clear winner. Prizes, if any are offered, should be based on number of match wins, or match points earned.

Download this repository and open index.html in any web browser -- no webserver needed!  Or, visit the live web site at [housedraft.games](https://housedraft.games/).  There's no account to create, and all of your tournament data stays on your computer.

## Screenshots

![Sample pairings screen](images/pairings.png)

![Sample standings screen](images/standings.png)
