<!doctype html>
<html lang="en">
<meta charset="utf-8">
<head>
<title>HouseDraft - About</title>
<link rel="stylesheet" type="text/css" href="css/hd_5.2.0.css">
<script type="text/javascript" src="js/jquery-3.7.0.min.js"></script>
<script type="text/javascript" src="js/help_4.1.0.js"></script>
<script type="text/javascript" src="js/enhanced_mode_5.0.0.js"></script>
<link rel="shortcut icon" href="favicon.png" type="image/x-icon">
<link rel="canonical" href="https://housedraft.games/about">
</head>
<body class="about">

<header>
<h1><a href="index.html">HouseDraft</a></h1>
<nav>
<a href="instructions.html">instructions</a>
<a href="about.html" class="same_page_link">about</a>
<a href="extras.html">extras</a>
<a href="index.html">home</a>
</nav>
</header>

<main class="help">
<section id="about">

<h2>About HouseDraft</h2>

<p>
If you want to run a small,
<a href="https://en.wikipedia.org/wiki/Swiss-system_tournament">Swiss-style game tournament</a>,
HouseDraft can help you with that.  The name comes from the idea of running a
<i><a href="https://magic.wizards.com/">Magic: The Gathering</a></i> draft in your house,
and the tiebreakers used are those recommended by
<a href="https://www.wizards.com/default.asp?x=dci/welcome">the DCI</a>.
But the site is "game-agnostic" and can be used to run tournaments for other formats or other games.
</p>

<p>
The benefit of a Swiss-style tournament is that players don't get eliminated.  Every player can continue playing
until the tournament is over, and will be ranked according to how well they do, and paired against
opponents of similar rank (but never the same opponent twice).  The downside (if you consider it one)
is that there may not be a single clear winner.  Prizes, if any are offered, should be based on
number of match wins, or match points earned.
</p>

<p>
HouseDraft is developed and maintained by <a href="https://triskaideka.net/">Triskaideka</a>.
Its <a href="https://gitlab.com/Triskaideka/housedraft">source code</a> is publicly available.
</p>

</section>
<section id="questions">

<h2>Preemptively Answered Questions</h2>

<ol class="toc"></ol>

<dl>

<dt id="pairing_consistency">Are the pairings the same ones I'd get if I used Wizards Event Reporter (or any other piece of software)?</dt>

<dd>They might not be exactly the same.  While HouseDraft follows the general rules for pairing Swiss tournaments,
any other software you may be familiar with could have particular quirks that this site does not reproduce.</dd>


<dt id="byes">What if I have an odd number of players?</dt>

<dd>In any round where the number of players remaining is uneven, one player will be assigned a "bye", which means
that they get a free win but they don't play a match.  The same player will never get a bye twice in one
tournament.</dd>

<dd>An even number of players is better, since everyone gets to play in every round, but don't let an odd number
deter you from running your tournament!</dd>


<dt id="tournament_length">How many rounds should my tournament last?</dt>

<table class="number_of_rounds_table">
<tr><th scope="col"># players</th><th scope="col"># rounds</th></tr>
<tr><td>3&ndash;4</td><td>2</td></tr>
<tr><td>5&ndash;8</td><td>3</td></tr>
<tr><td>9&ndash;16</td><td> 4</td></tr>
<tr><td>17&ndash;32</td><td>5</td></tr>
<tr><td>33&ndash;64</td><td>6</td></tr>
<tr><td>65&ndash;128</td><td> 7</td></tr>
<tr><td>129&ndash;226</td><td>8</td></tr>
<tr><td>227&ndash;409</td><td>9</td></tr>
<tr><td>410+</td><td>10</td></tr>
</table>

<dd>The table at right, based on Appendix E of the <i>Magic: The Gathering</i> Tournament Rules,
should more than cover any number of people you can fit in your house.  (You should be able to
find the Tournament Rules on the Wizards Play Network's
<a href="https://wpn.wizards.com/en/rules-documents">Rules and Documents page</a>.)</dd>

<dd>(If you're mathematically inclined: play <kbd>ceil(log<sub>2</sub>(<var>p</var>))</kbd>
rounds, where <var>p</var> is the number of players.)</dd>

<dd>You <i>may</i> be able to get away with playing extra rounds, although the system will be forced to start pairing players who have dissimilar records.  For example, you <i>can</i> run a four-round tournament with eight players (and I've done so, many times).  But at some point, it will become impossible to find an arrangement in which everybody is facing somebody they haven't played against yet.  This web site will refuse to pair another round when it reaches that point.</dd>

<dd>You can always choose to play fewer rounds if you wish.  You may <i>have</i> to play fewer rounds if some of your players drop out early.</dd>

<dd>You should also consider that, while playing games is fun, most people tend to run out of steam after a few
hours.  If you're planning a long haul, make some time in the schedule for a lunch break.</dd>


<dt id="tournament_size">How big a tournament can I run with this site?</dt>

<dd>That's up to you.  No upper limit is enforced, but you might find that the interface becomes awkward
to use once the list of players is too long to fit on your screen.  At some point you'll run into the limits
on how much data the site can store in your web browser, but if you're running a tournament that large,
you should really find some software that's designed for that purpose.</dd>


<dt id="storage">What's that about storing data in my browser?</dt>

<dd>HouseDraft stores your tournament data using the
<a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API">Web Storage API</a>,
which is technically different from "cookies", but conceptually the same, in that it is a
small amount of data that a remote site stores on your computer.  HouseDraft does not store any
information that is unrelated to organizing the tournament.  It does not store this data
on a remote server, nor does it make this data available to any third parties.
(At the moment it doesn't even transmit data across the internet at all, though that might change
if needed to support new features.)</dd>

<dd>HouseDraft stores data so that you won't lose it all
if your computer crashes.  If you block this site's access to local storage,
you'll (probably) still be able to run a tournament, but you'll lose the records if you close your
browser window or navigate away from the main page.</dd>

<dd>After your tournament ends, if you don't want its data to remain in your local storage, you can click the
<strong>Start a new tournament</strong> button, which will replace it with a blank tournament.  Or, you
can go into your browser's settings and clear it out entirely.</dd>


<dt id="tiebreakers">I think tiebreakers are being calculated wrong.</dt>

<dd>Before you submit a bug report about tiebreakers, there are a couple of oddities you should know about.</dd>

<dd>This site follows the guidance of the <i>Magic: The Gathering</i> Tournament Rules (MTR), which say to
treat all tiebreakers lower than 33.333% as if they were 33.333%, because it "limits the effect low
performances have when calculating and comparing opponents’ match-win percentage."  This affects
not only that player's tiebreakers but also their opponents' tiebreakers.</dd>

<dd>Further, while the names of the tiebreakers refer to "game win percentage" or "match win percentage",
the MTR actually say to calculate them using game or match <em>points</em>.  Thus, draws may affect players'
tiebreakers in a way you don't expect.  Additionally, if you use this site's feature that lets you change
the number of points awarded for a win or draw, it may affect the tiebreaker calculations.</dd>

<dd>If you need it for reference, you should be able
to find a copy of the latest tournament rules at the Wizards Play Network's
<a href="https://wpn.wizards.com/en/rules-documents">Rules and Documents</a> page.
Tiebreakers are covered in Appendix C.</dd>


<dt id="simultaneous_tournaments">What if I need to run two or more tournaments concurrently?</dt>

<dd>At the moment, my best advice is to open two browsers&mdash;not just two windows, but two
completely different web browsers, e.g. Chrome and Firefox.  Or you could even use two different
computers, side-by-side, if that's an option.  I <em>don't</em> recommend using HouseDraft in your
browser's "Incognito" or "Private Browsing" mode, because your data will be lost if the browser
window closes or the computer reboots.</dd>


<dt id="showstopper">I encountered a bug and now the site doesn't work.</dt>

<dd>Go to <a href="extras.html">the extras page</a> and click the link to clear out your data, then try again.
If it still doesn't work, please <a href="https://gitlab.com/Triskaideka/housedraft/-/issues">submit
a bug report</a>!</dd>

</dl>

</section>
</main>

</body>
</html>
