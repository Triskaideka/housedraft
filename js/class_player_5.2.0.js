/*
 * class_player.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * ----------------------------------------------------------------------------
 * HouseDraft's Tournament, Round, and Player objects are symbiotic, and freely
 * make use of each other's properties.  If you're trying to reuse this code
 * for something, note that you won't be able to take just one of these classes
 * and have it work without the others.
 * ----------------------------------------------------------------------------
 *
 * The "Player" class features the properties and methods needed to represent a
 * player in a tournament.
 *
 * Tiebreakers are calculated according to the instructions in Appendix C of
 * the Magic: The Gathering Tournament Rules (herein referred to as "MTR"),
 *
 * Wizards keeps changing the address of the MTR, but see if you can find them
 * in one of these places:
 *
 * https://media.wizards.com/2023/wpn/marketing_materials/wpn/mtg_mtr_2023may29_en.pdf
 * https://wpn.wizards.com/en/rules-documents
 * https://wpn.wizards.com/
 */

/**
 * Return a player object with the specified name, or based on the specified
 * object.  This function is called with an object when that object is loaded
 * from localStorage.  You can't serialize methods in JSON, which is why you
 * would pass the loaded object to this function instead of just using it as-is.
 *
 * @param {object|string} base    Player object to load, or name to use for constructing a new Player.
 * @param {number}        id_num  A sequential ID number, unique within a given Tournament.
 */
function Player (base, id_num = null) {

  if ( typeof base === "object" ) {

    // copy each property from the base object into the one we're creating
    for (let property in base) {
      // I wrote this code a while ago, but I think the reason for
      // "hasOwnProperty" is to distinguish the properties it has because it's a
      // Player from the many properties it has automatically because it's an object.
      if ( base.hasOwnProperty(property) ) {
        this[property] = base[property];
      }
    }

  } else if ( typeof base === "string" ) {

    // The familiar name that we'll use to refer to this player in
    // standings and pairings.
    this.name = base;



    // Check for a missing ID number.
    if (typeof id_num !== "number") {
      console.error("You created a new Player with a name, but didn't provide a valid ID number.");
    }

    // But use whatever we got for the ID number, anyway.
    this.pid = id_num;



    // The number of decimal places to show when displaying tiebreakers.
    // Arguably this should be stored in the Tournament class, but it's only
    // used by methods in this class, so here it is.
    this.tb_display_precision = 3;

    // If the player has dropped, this property indicates the last round that
    // they participated in.
    //
    // It's possible for a player to drop, re-join, and then drop again.
    // This property only indicates the *last* time that they dropped.
    // If you need more detailed information about their comings and goings,
    // you might be able to find it by examining the Tournament.rounds array.
    this.dropped_after_round = null;

    // If this player needs to remain at the same table throughout the
    // tournament, we'll give them a fixed table number.
    this.ftn = null;

  } else {

    console.error("Sent the wrong type of argument to the Player object constructor.");

  }



  // Functions can't be serialized into JSON, so they can't be stored when the Player object is saved into
  // localStorage.  So, we add them here, after we've set up the object properties based on whatever
  // the argument to this function was.
  this.calculate_gwp = calculate_gwp;
  this.calculate_mwp = calculate_mwp;
  this.calculate_ogw = calculate_ogw;
  this.calculate_omw = calculate_omw;

  this.count_games_drawn = count_games_drawn;
  this.count_games_played = count_games_played;
  this.count_games_won = count_games_won;

  this.count_matches_drawn = count_matches_drawn;
  this.count_matches_played = count_matches_played;
  this.count_matches_won = count_matches_won;

  this.drop = drop;
  this.dropped = dropped;

  this.ever_paired = ever_paired;

  this.get_displayable_gw = get_displayable_gw;
  this.get_displayable_ogw = get_displayable_ogw;
  this.get_displayable_omw = get_displayable_omw;

  this.get_game_points = get_game_points;
  this.get_match_points = get_match_points;

  this.list_opponent_ids = list_opponent_ids;

  this.set_ftn = set_ftn;

  this.reinstate = reinstate;
  this.undrop = reinstate;  // a little harmless redundancy, just in case I forget what I called it

}



/**
 * Calculate this player's game win percentage.
 *
 * MTR: "Similar to the match-win percentage, a player’s game-win percentage is
 * the total number of game points they earned divided by the total game points
 * possible (generally, 3 times the number of games played). Again, use 0.33 if
 * the actual game-win percentage is lower than that."
 *
 * It's worth pointing out that game win percentage is actually calculated using
 * game *points* rather than just *games*.  Other tiebreakers work the same way.
 *
 * @param  {number}  as_of_round  The latest round to include in the calculation.
 * @return {number}               This player's game win percentage.
 */
function calculate_gwp(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let actual_gwp = 0
    , possible_game_points = tournament.points_for_a_win * this.count_games_played(as_of_round)
    ;

  // Avoid dividing by zero.
  if (possible_game_points > 0) {
    actual_gwp = this.get_game_points(as_of_round) / possible_game_points;
  }

  // Return the player's game win percentage, or the tiebreaker floor if it is higher.
  return (actual_gwp >= tournament.tb_floor ? actual_gwp : tournament.tb_floor);

}  // end function calculate_gwp()



/**
 * Calculate this player's match win percentage.
 *
 * MTR: "A player’s match-win percentage is that player’s accumulated match
 * points divided by the total match points possible in those rounds (generally,
 * 3 times the number of rounds played). If this number is lower than 0.33, use
 * 0.33 instead. The minimum match-win percentage of 0.33 limits the effect low
 * performances have when calculating and comparing opponents’ match-win
 * percentage."
 *
 * "A player’s byes are ignored when computing their opponents’ match-win and
 * opponents’ game-win percentages."
 *
 * Note that this means that MWP is *not* the same as matches won divided by
 * matches played, because MWP takes draws into account, and they're worth one
 * third of a match win (usually, though we let the user change that).
 *
 * @param  {number}  as_of_round  The latest round to include in the calculation.
 * @return {number}               This player's match win percentage.
 */
function calculate_mwp(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let actual_mwp = 0
    , possible_match_points = tournament.points_for_a_win * this.count_matches_played(as_of_round)
    ;

  // Avoid dividing by zero.
  if (possible_match_points > 0) {
    actual_mwp = this.get_match_points(as_of_round) / possible_match_points;
  }

  // Return the player's match win percentage, or the tiebreaker floor if it is higher.
  return (actual_mwp >= tournament.tb_floor ? actual_mwp : tournament.tb_floor);

}  // end function calculate_mwp()



/**
 * Calculate this player's opponents' game win percentage.
 *
 * MTR: "Similar to opponents’ match-win percentage, a player’s opponents’
 * game-win percentage is simply the average game-win percentage of all that
 * player’s opponents. And, as with opponents’ match-win percentage, each
 * opponent has a minimum game-win percentage of 0.33."
 *
 * MTR: "A player’s byes are ignored when computing their opponents’ match-win
 * and opponents’ game-win percentages."
 *
 * @param  {number}  as_of_round  The latest round to include in the calculation.
 * @return {number}               This player's opponents' game win percentage.
 */
function calculate_ogw(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  // Find each opponent's GWP, average them, and return that average.
  // If the player has had no opponents yet, return the tiebreaker floor value
  // instead.
  let opponents = this.list_opponent_ids(as_of_round, false);

  if (opponents.length < 1) {

    return tournament.tb_floor;

  } else {

    return (
      _.chain(opponents)
        .map(function(id){ return tournament.get_player_by_id(id).calculate_gwp(as_of_round); })  // turn ID numbers into stats
        .reduce(function(memo,sum){ return memo + sum; }, 0)  // add those stats up
      .value() / opponents.length  // and then average them
    );

  }


}  // end function calculate_ogw()



/**
 * Calculate this player's opponents' match win percentage.
 *
 * MTR: "A player’s opponents’ match-win percentage is the average match-win
 * percentage of each opponent that player faced (ignoring those rounds for
 * which the player received a bye)."
 *
 * MTR: "A player’s byes are ignored when computing their opponents’ match-win
 * and opponents’ game-win percentages."  This sentence is ambiguous, but here's
 * my interpretation:
 *
 * There's an MTR quote in the comments on calculate_mwp() that makes it clear,
 * I think, that byes do factor into a player's *own* match win percentage.
 *
 * When you go looking for the MWP for each of a player's opponents, in order to
 * average them for this function, you don't consider the Bye player to be one
 * of their opponents.  But, when you run calculate_mwp() for each of those
 * opponents, it will still include the Bye player as usual.
 *
 * @param  {number}  as_of_round  The latest round to include in the calculation.
 * @return {number}               This player's opponents' match win percentage.
 */
function calculate_omw(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  // Find each opponent's MWP, average them, and return that average.
  // If the player has had no opponents yet, return the tiebreaker floor value
  // instead.
  let opponents = this.list_opponent_ids(as_of_round, false);

  if (opponents.length < 1) {

    return tournament.tb_floor;

  } else {

    return (
      _.chain(opponents)
        .map(function(id){ return tournament.get_player_by_id(id).calculate_mwp(as_of_round); })  // turn ID numbers into stats
        .reduce(function(memo,sum){ return memo + sum; }, 0)  // add those stats up
      .value() / opponents.length  // and then average them
    );

  }

}  // end function calculate_omw()



/**
 * Report how many games the player has drawn.
 *
 * It makes logical sense for this method, and the others like it, to belong to
 * the Player object, but it uses data that's stored in the Tournament object.
 * It might be better for this to be a method of Tournament -- there are pros
 * and cons to either approach.  I'm settling on this for now.
 *
 * @param  {number} as_of_round The latest round to consider.
 * @return {number}             Number of games the player has played in the tournament that ended in a draw, up to & including the indicated round.
 */
function count_games_drawn(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let games_drawn = 0;

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    let match = _.find(tournament.rounds[r].matches, function(m){
      return (m.players[0].pid == player_id || m.players[1].pid == player_id);
    });

    if (match) {
      games_drawn += match.draws;
    }

  }

  return games_drawn;

}  // end function count_games_drawn()



/**
 * Report how many games the player has played.
 *
 * @param  {number}  as_of_round   The latest round to consider.
 * @param  {Boolean} consider_byes Whether or not to include games against the Bye player in the calculation.
 * @return {number}                Number of games the player has played in the tournament, up to & including the indicated round.
 */
function count_games_played(as_of_round = null, consider_byes = true) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let games_played = 0;

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    let match = _.find(tournament.rounds[r].matches, function(m){
      return (m.players[0].pid == player_id || m.players[1].pid == player_id);
    });

    if (match) {

      if ( match_contains_bye(match) ) {

        if (consider_byes) {
          games_played += tournament.games_to_win_a_match;
        }

        // But if the "opponent" is the Bye, and we're not considering byes,
        // then don't add anything.

      } else {  // Opponent is not the Bye.

        // Every game counts as a game played, no matter who (if anyone) won it.
        games_played += match.players[0].wins;
        games_played += match.players[1].wins;
        games_played += match.draws;

      }

    }

  }

  return games_played;

}  // end function count_games_played()



/**
 * Report how many games the player has won.
 *
 * MTR: "When a player is assigned a bye for a round, they are considered to
 * have won the match 2–0."
 *
 * @param  {number}  as_of_round   The latest round to consider.
 * @param  {Boolean} consider_byes Whether or not to include wins against the Bye player in the calculation.
 * @return {number}                Number of games the player has won in the tournament, up to & including the indicated round.
 */
function count_games_won(as_of_round = null, consider_byes = true) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let games_won = 0;

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    let match = _.find(tournament.rounds[r].matches, function(m){
      return (m.players[0].pid == player_id || m.players[1].pid == player_id);
    });

    if (match) {

      if ( match_contains_bye(match) ) {

        if (consider_byes) {
          games_won += tournament.games_to_win_a_match;
        }

        // But if the "opponent" is the Bye, and we're not considering byes,
        // then don't add anything.

      } else {  // Opponent is not bye.

        // Find this player in that match.
        let player_record = _.find(match.players, function(p){
          return (p.pid === player_id);
        });

        games_won += player_record.wins;

      }

    }

  }

  return games_won;

}  // end function count_games_won()



/**
 * Report how many matches the player has drawn.
 *
 * A match is considered to have been drawn if both of its players won the same
 * number of games in it, regardless of what that number is, and regardless of
 * how many games they drew.
 *
 * @param  {number} as_of_round The latest round to consider.
 * @return {number}             Number of matches the player has played in the tournament that ended in a draw, up to & including the indicated round.
 */
function count_matches_drawn(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let matches_drawn = 0;

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    let match = _.find(tournament.rounds[r].matches, function(m){
      return (m.players[0].pid == player_id || m.players[1].pid == player_id);
    });

    // (A bye can never be a draw.)
    if ( match && !match_contains_bye(match) ) {

      // Regardless of the number of games drawn (even if it's 0),
      // the match is a draw if both players have WON the same number of games.
      if (match.players[0].wins === match.players[1].wins) {
        matches_drawn++;
      }

    }

  }

  return matches_drawn;

}  // end function count_matches_drawn()



/**
 * Report how many matches the player has played.
 *
 * @param  {number}  as_of_round   The latest round to consider.
 * @param  {Boolean} consider_byes Whether or not to include matches against the Bye player in the calculation.
 * @return {number}                Number of matches the player has played in the tournament, up to & including the indicated round.
 */
function count_matches_played(as_of_round = null, consider_byes = true) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let matches_played = 0;

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    if ( _.any(tournament.rounds[r].matches, function(m){
                return (
                  ( m.players[0].pid == player_id || m.players[1].pid == player_id ) &&
                  ( consider_byes || (m.players[0].pid !== 0 && m.players[1].pid !== 0) )
                );
              })
    ) {
      matches_played++;
    }

  }

  return matches_played;

}  // end function count_matches_played()



/**
 * Report how many matches the player has won.
 *
 * @param  {number}  as_of_round   The latest round to consider.
 * @param  {Boolean} consider_byes Whether or not to include wins against the Bye player in the calculation.
 * @return {number}                Number of matches the player has won in the tournament, up to & including the indicated round.
 */
function count_matches_won(as_of_round = null, consider_byes = true) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let matches_won = 0;

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    let match = _.find(tournament.rounds[r].matches, function(m){
      return (m.players[0].pid == player_id || m.players[1].pid == player_id);
    });

    if (match) {

      if ( match_contains_bye(match) ) {

        if (consider_byes) {
          matches_won++;
        }

        // But if the "opponent" is the Bye, and we're not considering byes,
        // then don't add anything.

      } else {  // Opponent is not bye.

        // It's only a match win if this player has MORE game wins than their opponent.
        if (   (match.players[0].pid == this.pid && match.players[0].wins > match.players[1].wins)
            || (match.players[1].pid == this.pid && match.players[1].wins > match.players[0].wins)
        ) {
          matches_won++;
        }

      }

    }

  }

  return matches_won;

}  // end function count_matches_won()



/**
 * Mark a player as having dropped after the indicated round, but keep them in
 * the player list.
 *
 * Contrast with Tournament.delete_player().
 *
 * @param  {number} after_round The number of the last round that the player participated in.
 * @return {void}
 */
function drop(after_round = null) {

  if ( this.dropped() ) {
    console.warn("Called drop() for a player that was already dropped.");
  }

  // Validate the arguments.
  if (   after_round === null
      || after_round >= tournament.rounds.length
  ) {
    after_round = tournament.get_lrn();
  }

  this.dropped_after_round = after_round;

}



/**
 * Return true or false depending on whether the player has dropped from the
 * tournament.
 *
 * @return {Boolean} True if the player has dropped from the tournament, false if they have not.
 */
function dropped() {
  return ( this.dropped_after_round !== null );
}



/**
 * Returns true if the player has been a participant in any match in the
 * tournament, whether completed or ongoing.  Note that this is different from
 * checking whether count_games_played() is > 0, because this could be true if
 * the player is still in the middle of (or sitting down for) their first game.
 *
 * This method was written specifically to accommodate the needs of
 * Tournament.delete_player().  See the comments on that method for more about
 * why this is important.
 *
 * @return {Boolean} True if the player was ever paired, false if not.
 */
function ever_paired() {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  let id = this.pid;

  return _.any(
    tournament.rounds,
    function(r) {
      return _.any(
        r.matches,
        function(m){
          return (
            m.players[0].pid === id ||
            ( m.players.length > 1 && m.players[1].pid === id )
          );
        }
      )
    }
  );

}  // end function ever_paired()



/**
 * Return the player's GW tiebreaker, in the format that we want to use when
 * displaying it in the standings table.
 *
 * @param  {number} as_of_round The latest round to include in the calculation.
 * @return {string}             The player's game win percentage, in displayable format.
 */
function get_displayable_gw(as_of_round = null) {

  // Check for errors.
  if (this.pid < 1) {
    console.error("Tried to get_displayable_gw() for PID " + this.pid);
    return null;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }

  return (this.calculate_gwp(as_of_round) * 100).toFixed(this.tb_display_precision);

}



/**
 * Return the player's OGW tiebreaker, in the format that we want to use when
 * displaying it in the standings table.
 *
 * @param  {number} as_of_round The latest round to include in the calculation.
 * @return {string}             The player's opponents' game win percentage, in displayable format.
 */
function get_displayable_ogw(as_of_round = null) {

  // Check for errors.
  if (this.pid < 1) {
    console.error("Tried to get_displayable_ogw() for PID " + this.pid);
    return null;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }

  return (this.calculate_ogw(as_of_round) * 100).toFixed(this.tb_display_precision);

}



/**
 * Return the player's OMW tiebreaker, in the format that we want to use when
 * displaying it in the standings table.
 *
 * @param  {number} as_of_round The latest round to include in the calculation.
 * @return {string}             The player's opponents' match win percentage, in displayable format.
 */
function get_displayable_omw(as_of_round = null) {

  // Check for errors.
  if (this.pid < 1) {
    console.error("Tried to get_displayable_omw() for PID " + this.pid);
    return null;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }

  return (this.calculate_omw(as_of_round) * 100).toFixed(this.tb_display_precision);

}



/**
 * Report how many "game points" the player has earned based on their game wins and draws.
 *
 * MTR: "When a player is assigned a bye for a round, they are considered to
 * have won the match 2–0.  Thus, that player earns 3 match points and 6 game
 * points."
 *
 * MTR: "Game points are not used in team tournaments; only the overall result
 * of the match is used for tiebreakers."  HouseDraft does not currently
 * differentiate between team and individual tournaments, but that may be a
 * feature enhancement for the future.
 *
 * @param  {number}  as_of_round  The latest round to include in the calculation.
 * @return {number}               The player's game points earned.
 */
function get_game_points(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  return ( this.count_games_won(as_of_round) * tournament.points_for_a_win ) +
         ( this.count_games_drawn(as_of_round) * tournament.points_for_a_draw );

}  // end function get_game_points()



/**
 * Report how many "match points" the player has earned based on their match
 * wins and draws.
 *
 * MTR: "Players earn 3 match points for each match win, 0 points for each match
 * loss and 1 match point for each match ending in a draw. Players receiving
 * byes are considered to have won the match."
 *
 * @param  {number}  as_of_round  The latest round to include in the calculation.
 * @return {number}               The player's match points earned.
 */
function get_match_points(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return null;
  }

  // Always return 0 for the Bye player.
  if (this.pid === 0) {
    return 0;
  }

  return ( this.count_matches_won(as_of_round) * tournament.points_for_a_win ) +
         ( this.count_matches_drawn(as_of_round) * tournament.points_for_a_draw );

}  // end function get_match_points()



/**
 * Return a list of player IDs that enumerates who this player has faced so far
 * in this tournament.  Includes the Bye ID, if the player had a bye.
 *
 * @param  {number}  as_of_round     Only list players faced during or prior to this round.
 * @param  {Boolean} include_the_bye Whether or not to include the Bye player in the returned list, if this player received a bye.
 * @return {array}                   An array of player IDs.
 */
function list_opponent_ids(as_of_round = null, include_the_bye = false) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("Called Player.list_opponent_ids() at a time when no Tournament exists.");
    return null;
  }

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = tournament.get_lrn();
  }



  let opponents = [];

  for (let r = 0; r <= as_of_round; r++) {

    let player_id = this.pid;

    // Find the match that has this player in it.
    let match = _.find(tournament.rounds[r].matches, function(m){
      return (m.players[0].pid == player_id || m.players[1].pid == player_id);
    });

    if (   match
        && ( include_the_bye || !match_contains_bye(match) )
    ) {
      // Figure out which player in the match was the opponent, and add their
      // player ID to the list.
      if (match.players[0].pid == this.pid) {
        opponents.push(match.players[1].pid);
      } else if (match.players[1].pid == this.pid) {
        opponents.push(match.players[0].pid);
      } else {
        console.error(`This match is supposed to include player ${this.pid}, but it doesn't seem to.`);
      }
    }

  }

  return opponents;

}  // end function list_opponent_ids()



/**
 * Report whether or not the given match, i.e. an object in the format used by
 * class_round.js, features a player whose player ID is 0, which means that they
 * are the Bye.
 *
 * This is a static function, not a method of Player, and so perhaps should be
 * stored elsewhere, but for now, it's only used here, and so it's convenient
 * to store it here.
 *
 * @param  {object}  match An object containing match data, in the format used by the Round class.
 * @return {boolean}       Whether or not one of the players in the match is the Bye.
 */
function match_contains_bye(match) {

  // Error checking.
  if (   typeof match !== "object"
      || ! match.hasOwnProperty("players")
      //|| typeof match.players !== "array"
  ) {
    console.error("You sent something to match_contains_bye() that was not a match object:");
    console.error(match);
    return false;
  }

  if ( _.any( match.players, function(p){ return p.pid === 0; } ) ) {
    return true;
  } else {
    return false;
  }

}



/**
 * Set a fixed table number (FTN) for the player.
 *
 * If for some reason it is desirable to have a certain player remain at the
 * same table for the entire tournament, regardless of their record, you can
 * use this feature to "fix" their table number, i.e. prevent it from changing.
 *
 * If you mark and clear several checkboxes, depending on the order you do it
 * in, it's possible to end up with a situation where the FTNs assigned are
 * not consecutive.  Arguably we should run some sort of clean-up code when
 * the checkbox is cleared.  Except if we did that, then unassigning one
 * player's FTN mid-tournament could change another player's FTN that is
 * already in use.  And we don't want that.  So, maybe there's a more elegant
 * solution to all this, but for now, this code is fine & functional.
 *
 * @param {Boolean} status Whether we are fixing the player's table number (true) or stopping it from being fixed (false).
 * @return {integer} The player's fixed table number, or null if their table number is not fixed.
 */
function set_ftn(status = true) {

  // If we're removing the player's FTN:
  if ( ! status ) {

    this.ftn = null;

  // If we're establishing the player's FTN:
  } else {

    // First get a list of the FTNs that are already in use.
    let existing_ftns = _.chain(tournament.players)
      .map( function(p){ return p.ftn; } )
      .reject( function(n){ return _.isNull(n); } )
      .value();

    // What's the highest number on that list?
    let highest_ftn = (existing_ftns.length > 0) ? _.max(existing_ftns) : 0;

    // Make this player's FTN the lowest integer that's not already in use.
    for (let n = 1; n <= highest_ftn + 1; n++) {
      // If this number is not already somebody else's FTN, then use it.
      if ( _.indexOf(existing_ftns, n) === -1 ) {
        this.ftn = n;
        break;
      }
    }

  }

  return this.ftn;

}  // end function set_ftn()



/**
 * Return a dropped player to the tournament.
 *
 * @return {void}
 */
function reinstate() {
  if ( ! this.dropped() ) { console.warn("Called reinstate() for a player that was not dropped."); }
  this.dropped_after_round = null;
}
