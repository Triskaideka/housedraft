/*
 * class_round.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * ----------------------------------------------------------------------------
 * HouseDraft's Tournament, Round, and Player objects are symbiotic, and freely
 * make use of each other's properties.  If you're trying to reuse this code
 * for something, note that you won't be able to take just one of these classes
 * and have it work without the others.
 * ----------------------------------------------------------------------------
 *
 * The "Round" class features the properties and methods needed to store
 * settings and other information for one round of a tournament.
 *
 * The structure of each element in Round.matches is:
 *
 *  {
 *    "players": [
 *      {"pid": 7, "wins": 0},
 *      {"pid": 8, "wins": 0},
 *    ],
 *    "draws": 0,
 *    "table_number": 1
 *  }
 *
 * In the future, Round might have a property called something like
 * "structure" or "type".  This would help if there's ever a feature where you
 * can cut to a Top N single-elimination bracket at the end of the tournament.
 */

// Return a fresh Round object.
// This function is called with an object when that object is loaded from localStorage.
function Round (base = null) {

  // Functions can't be serialized into JSON, so they can't be stored when the
  // Tournament object is saved into localStorage.  So, we add them explicitly
  // every time we create a Round, whether it's a new one or not.
  this.add_matches = add_matches;
  this.all_matches_have_results = all_matches_have_results;
  this.anybody_has_a_bye = anybody_has_a_bye;
  this.determine_pairings = determine_pairings;
  this.get_matches = get_matches;
  this.get_pairings_tbody = get_pairings_tbody;
  this.has_any_results = has_any_results;
  this.pair_unpaired_players = pair_unpaired_players;



  /* Default values */

  this.matches = [];

  // Outside of this class, you probably already need to know the round's number
  // before you access it, but inside of the class, there might not be an easy
  // way of getting it.  So we store it in the object.  (It's null now, but it
  // will be set a few lines down.)
  this.number = null;



  // Was there an object to load?
  if ( base !== null ) {

    // interesting fact: typeof null === "object"
    // see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof

    if (typeof base === "object") {

      // copy each property from the base object into the one we're creating
      for (let property in base) {
        if ( base.hasOwnProperty(property) ) {
          this[property] = base[property];
        }
      }

    } else {
      console.error("Sent the wrong type of argument (" + typeof base + ") to the Round object constructor.");
    }

  // If we didn't load an object, and are instead creating a new round:
  } else {

    if (typeof tournament === "undefined") {
      console.error("Created a new round, but couldn't figure out what round number to give it.");
    } else {
      this.number = tournament.rounds.length;
    }

  }

}



/**
 * Given a list of pairings, create match objects for each of those
 * pairings, and add them to this round's matches.
 *
 * @param {array} pairings An array of two-element arrays in which each element is a Player object.
 * @return {void}
 */
function add_matches(pairings) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return false;
  }



  // Figure out what table number to start with.
  let table_number = tournament.first_table_num;

  if (this.matches.length > 0) {

    table_number = _.chain(this.matches)
      .map(function(m){ return m.table_number; })
      .max()
      .value() + 1;  // Start one higher than the current highest table number in this round.

  }



  // Iterate over the pairings.
  for (let pairing of pairings) {

    // This code finds out whether there is a fixed table number that we should
    // use for this match, accounting for the possibility that *both* players
    // will have FTNs, in which case we'll choose the lowest.  Note that
    // _.min() will return Infinity if *neither* player has an FTN, which we
    // account for in the code a few lines down.
    let fixed_table_number = _.min(
      _.map(
        pairing,
        function(p){ return p.ftn; }
      )
    );

    // Create and add the match object.
    this.matches.push({
      "players": [
        {"pid": pairing[0].pid, "wins": 0},
        {"pid": pairing[1].pid, "wins": 0},
      ],
      "draws": 0,
      "table_number": (fixed_table_number < Infinity) ? 'F' + fixed_table_number : table_number++
    });

  }

}  // end function add_matches()



/**
 * Detect whether every match in this round has some result reported for it.
 *
 * Note that this function isn't concerned with the value of
 * tournament.games_to_win_a_match.  It is normal for some matches to end after
 * only a single game, because time ran out.
 *
 * @return {Boolean} True if EVERY match in the round has at least one win or draw, false if there are any that don't.
 */
function all_matches_have_results() {
  return _.all(this.matches, function(m){
    return (
      m.draws > 0                                          // If there was at least one drawn game,
      || m.players[0].wins > 0  || m.players[1].wins > 0   // or someone won at least one game,
      || m.players[0].pid === 0 || m.players[1].pid === 0  // or one of the "players" was the bye.
    );
  });
}



/**
 * Tells you whether there is a bye in this round.
 *
 * There is some similarity to the static match_contains_bye() function in
 * the Player class, and maybe the code could be combined in some way, but
 * both functions are short and they're working fine.
 *
 * @return {Boolean} True if any player has a bye in this round, false if not.
 */
function anybody_has_a_bye() {
  return _.any(this.matches, function(m){
    return _.any( m.players, function(p){
      return p.pid === 0;
    });
  });
}



/**
 * Call at the start of a round, to find out which players will play against one
 * another in this round.
 *
 * Don't call this method directly; call Round.get_matches() and let it decide
 * whether to call this method.
 *
 * This method fills the "matches" object directly, and only returns a Boolean
 * value indicating success or failure (which isn't currently being used by
 * anything).
 *
 * @return {Boolean} True if the round was successfully paired, false if it wasn't.
 */
function determine_pairings() {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return false;
  }

  if ( this.has_any_results() ) {
    console.warn("Overwriting pairings for a round that already has some results recorded; is that intentional?");
  }



  let active_players = []
    , halfway = 0
    , pairings = []
    ;

  this.matches = [];



  // Random pairings for the first round.
  if (this.number === 0) {

    // There shouldn't be any dropped players prior before the first round has
    // begun, but in case there are somehow, we definitely want to remove them.
    active_players = _.reject(tournament.players, function(p){ return p.dropped(); });

    // If including the bye results in an uneven number of players, remove it.
    if (active_players.length % 2 == 1) {
      active_players = _.filter(active_players, function(p){ return p.pid !== 0; });
    }



    // If there's NO assigned seating:
    if ( _.isEmpty(tournament.draft_seating) ) {

      active_players = _.shuffle(active_players);

      // This is safe because we know the array will contain an even number of elements
      for (let i = 0; i < active_players.length; i += 2) {
        pairings.push([ active_players[i], active_players[i+1] ]);
      }



    // If there IS assigned seating:
    } else {

      // Again, we know the array will contain an even number of elements b/c we decided above
      // whether to remove the bye or keep it in.

      // if there's a player left over, give them the bye
      if (tournament.draft_seating.length % 2 == 1) {
        tournament.draft_seating.splice(
          _.random(0, tournament.draft_seating.length - 1),  // select a random position at which to insert
          0,  // the number of items to delete at that position (none)
          0  // the value to insert (0, the PID of the bye)
        );
      }

      halfway = Math.floor(tournament.draft_seating.length / 2);

      for (let i = 0; i < halfway; i++) {
        pairings.push([
          _.find(active_players, function(p){return p.pid == tournament.draft_seating[i]}),
          _.find(active_players, function(p){return p.pid == tournament.draft_seating[halfway + i]})
        ]);
      }

    }

  // Record-based pairings for every round after the first.
  } else {

    active_players = tournament.get_players_in_standings_order(this.number, true, false);
    // If the above line results in an odd number of players, try it again with the Bye excluded.
    if (active_players.length % 2 == 1) {
      active_players = tournament.get_players_in_standings_order(this.number, false, false);
    }

    pairings = pair(active_players);

    if (pairings == null) {
      show_dialog("unable_to_pair");
      return false;  // We don't have pairings, so there's nothing else to do in this function.
    }

  }



  // Sort the bye match to the end and the bye player to the right side of
  // that match.
  pairings = standardize_pairings(pairings);



  // Add the pairings to this round's list of matches.
  this.add_matches(pairings);



  // Report that we were successful.
  return true;

}  // end function determine_pairings()



/**
 * Return the matches object for this round, creating it first if it doesn't
 * already exist.
 *
 * @return {array} An array of match objects, as described in the comments at the top of this file.
 */
function get_matches() {

  if ( _.isEmpty(this.matches) ) {
    this.determine_pairings();
  }

  return this.matches;

}  // end function get_matches()



/**
 * Return the HTML code that goes in the <tbody> of the pairings table for this
 * round.  Does not include the opening or closing <tbody>...</tbody> tags.
 *
 * @return {string} A series of HTML elements.
 */
function get_pairings_tbody() {

  let code = '<tbody>';

  for (let match of this.get_matches()) {

    let leftp = tournament.get_player_by_id(match.players[0].pid)
      , rightp = tournament.get_player_by_id(match.players[1].pid)

      // Boolean: is one of the players in this pairing the bye?
      // If so, we don't show all of the win/draw fields.
      , bye = (rightp.pid === 0)
      ;

    code += `<tr>
        <td>${match.table_number}</td>
        <td><span class="points">(${leftp.get_match_points(this.number - 1)})</span> ${leftp.name}</td>
        <td>${ make_number_field({
            "data": { "win-pid": leftp.pid },
            "disabled": bye,
            "max": tournament.games_to_win_a_match,
            "min": 0,
            "value": ( bye ? tournament.games_to_win_a_match : match.players[0].wins )
        }) }</td>
        <td>${( bye ? "&nbsp;" : make_number_field({
            "data": { "draw-pids": `(${leftp.pid})(${rightp.pid})` },
            "min": 0,
            "value": match.draws
        }) )}</td>
        <td>${( bye ? "&nbsp;" : make_number_field({
            "data": { "win-pid": rightp.pid },
            "max": tournament.games_to_win_a_match,
            "min": 0,
            "value": match.players[1].wins
        }) )}</td>
        <td>${rightp.name}${ bye ? '' : ` <span class="points">(${rightp.get_match_points(this.number - 1)})</span>` }</td>
      </tr>`;

  }

  code += '</tbody>';

  return code;

}  // end function get_pairings_tbody()



/**
 * Detect whether any match results (even partial ones) have already been
 * reported for this round.
 *
 * Note that this doesn't consider a bye to be a "result".  The idea is to use
 * this to check whether the user has neglected to input any results, but for
 * the bye, there's nothing to input.
 *
 * @return {Boolean} True if AT LEAST ONE match in the round has one or more wins or draws, false if none do.
 */
function has_any_results() {
  return _.any(this.matches, function(m){
    return (m.draws > 0 || m.players[0].wins > 0 || m.players[1].wins > 0);
  });
}



/**
 * Call recursively to get a set of standings-based pairings.
 * Only send an array with an even number of elements to this function.
 * That means you should remove the bye first, if necessary.
 *
 * This is a static function, not a class method, but it's defined here because
 * the function that calls it is a class method.
 *
 * @param  {array} unpaired_players A list of Player objects that have not yet been paired for this round.
 * @return {array}                  A two-dimensional array of player pairings.
 */
function pair(unpaired_players) {

  // Handle error case:
  if ( unpaired_players.length % 2 !== 0 ) {
    console.error("Called pair() with an odd number of players!");
    console.log(unpaired_players);
    return null;
  }

  // The bottom of the recursion stack:
  if ( unpaired_players.length === 0 ) {
    return [];
  }

  // Start the loop at 1 because we're looking for players to pair with the player at 0
  for (let i = 1; i < unpaired_players.length; i++) {

           // Shouldn't have to check both, but it doesn't hurt to be thorough:
    if (   ! _.contains(unpaired_players[0].list_opponent_ids(tournament.get_lrn(), true), unpaired_players[i].pid)
        && ! _.contains(unpaired_players[i].list_opponent_ids(tournament.get_lrn(), true), unpaired_players[0].pid)
    ) {

      // Looks like we have a potential pair.
      // Make a copy of the array and take the two players we're now trying to
      // pair out of it.
      let remaining_unpaired_players = unpaired_players.slice();
      remaining_unpaired_players.splice(i,1);  // Splice out i first, because splicing out 0 will lower the element that was at i.
      remaining_unpaired_players.splice(0,1);

      // Recurse into the next level.
      let result = pair(remaining_unpaired_players);

      if (result !== null) {
        return [ [ unpaired_players[0], unpaired_players[i] ] ].concat(result);
      }
    }

    // If we've made it to the end of the loop without returning, then the
    // pairing attempt was unsuccessful.
    // We'll have to restart the loop and try pairing with a different player.
  }

  // If we made it through the loop without finding a pairing for this player,
  // give up and report a failure to the calling function.
  return null;

} // end function pair()



/**
 * Assuming that there are already some matches in this round, and also at
 * least two players in this round who have not dropped but also are not in
 * a match against a real player, work out legal pairings for those unpaired
 * players, if possible, and add those pairings to this round's matches
 * object.
 *
 * This is primarily for adding players who arrive late, but might also be
 * called if a player who had dropped re-enters the tournament during a later
 * round.
 *
 * Note that this method does not update the page display afterwards; the
 * calling function should do that.
 *
 * @return {Boolean} True if new pairings were successfully generated; false if not.
 */
function pair_unpaired_players() {

  let pairings = [];

  // Make this round available inside the filtering function that follows.
  // The value of "this" inside that function is not the same as it is
  // right here, so we need to assign it to something so we can access it
  // easily.
  let this_round = this;

  // Filter out the players who are already in a match.
  let unpaired_players = _.reject(

    // The "false, false" at the end excludes the bye (PID 0) and all players
    // who have dropped.  If there is a bye in this round, we'll add the
    // player who got it to the list momentarily.
    tournament.get_players_in_standings_order(this.number, false, false),

    function(p1){
      return _.any(
        this_round.matches,
        function(m){
          return _.any(
            m.players,
            function(p2){
              return (p1.pid === p2.pid);
            }
          )
        }
      );
    }
  );

  // If there are no unpaired players, return early.
  if (unpaired_players.length < 1) {
    console.warn(
      "Called Round.pair_unpaired_players() at a time when there were no "
      + "unpaired players to pair. This probably shouldn't have been possible, "
      + "but it's harmless."
    );
    return false;
  }



  // If a player has been paired against the bye, they need to be
  // included in the unpaired_players list.
  if ( this.anybody_has_a_bye() ) {

    let match_with_bye = _.find(
      this.matches,
      function(m){
        return _.any(
          m.players,
          function(p){
            return (p.pid === 0);
          }
        )
      }
    );

    // The bye always gets moved to the right side of its match;
    // see Round.determine_pairings().  So we can safely assume that
    // match_with_bye.players[0] is the real player.
    let player_with_bye = tournament.get_player_by_id(match_with_bye.players[0].pid);

    unpaired_players.push(player_with_bye)

  }



  // pair() needs to be sent an even number of players, so add the bye to even
  // it out if necessary.
  if ( unpaired_players.length % 2 === 1 ) {
    unpaired_players.push( tournament.get_player_by_id(0) );
  }



  // Work out a list of pairings for the unpaired players.
  // This will follow the usual rules about pairings; in particular, it will
  // not pair players who have faced each other already.  (That wouldn't be an
  // issue if we're just adding late arrivals, but it might if a player drops
  // and later re-joins the tournament.)
  pairings = pair(unpaired_players);



  // If pair() didn't work (return value was null), report that and
  // get out of this method.
  if (pairings == null) {
    show_dialog("no_way_to_pair_unpaired");
    return false;
  }



  // If there's a bye match already in this round, remove it.
  // We're about to add a new match to this round that has that player in it.
  // (It's possible that the new match will still be a bye for that player,
  // but even then, we don't want there to be two such matches in the round.)
  if ( this.anybody_has_a_bye() ) {

    this.matches = _.reject(
      this.matches,
      function(m){
        return _.any(
          m.players,
          function(p){
            return (p.pid === 0);
          }
        );
      }
    );

  }



  // Sort the bye match to the end and the bye player to the right side of
  // that match.
  pairings = standardize_pairings(pairings);



  // Merge the new matches into the existing matches for this round.
  this.add_matches(pairings);



  // Note: updating the page display doesn't happen here;
  // the calling function is responsible for that.

  return true;

} // end function pair_unpaired_players()



/**
 * Given an array that should contain pairings, move the bye match to the end
 * and the bye player to the right side of that match.
 *
 * This is a static function, not a class method.
 *
 * @param {array} pairings An array of arrays, the sub-arrays each containing player IDs.
 * @return {array} The same array, sorted in our standard order.
 */
function standardize_pairings(pairings) {

  // Sort the pairing with the bye to the end of the list
  // (if it isn't already there).
  pairings = _.sortBy(pairings, function(pairing){
    if (pairing[0].pid === 0 || pairing[1].pid === 0) { return 1; }
    else { return 0; }
  });

  // And now move the bye, if any, to the right side of its pairing.
  if ( pairings[pairings.length-1][0].pid === 0 ) {
    pairings[pairings.length-1] = pairings[pairings.length-1].reverse();
  }

  return pairings;

}
