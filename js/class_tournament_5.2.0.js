/*
 * class_tournament.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * ----------------------------------------------------------------------------
 * HouseDraft's Tournament, Round, and Player objects are symbiotic, and freely
 * make use of each other's properties.  If you're trying to reuse this code
 * for something, note that you won't be able to take just one of these classes
 * and have it work without the others.
 * ----------------------------------------------------------------------------
 *
 * The "Tournament" class features the properties and methods needed to store
 * settings and other information for a tournament.
 *
 * A "Tournament" object is expected to contain multiple "Round" and
 * "Player" objects.  See also class_round.js and class_player.js.
 *
 */

// Return a fresh Tournament object.
// This function is called with an object when that object is loaded from localStorage.
function Tournament (base = null) {

  /* Establish some defaults. */
  this.default_first_table_num = 1;
  this.default_games_to_win_a_match = 2;
  this.default_points_for_a_draw = 1;
  this.default_points_for_a_win = 3;
  this.default_show_points = false;
  this.default_show_dropped_players = true;

  // The MTR puts a floor of 0.33 on tiebreakers.  It does not say 0.333333...
  // repeating, but it also sets an example of truncating everything to two
  // decimal places.  I'm taking the liberty of assuming it means 1/3.
  this.default_tb_floor = 1 / 3;

  // Functions can't be serialized into JSON, so they can't be stored when the
  // Tournament object is saved into localStorage.  So, we add them explicitly
  // every time we create a tournament, whether it's a new one or not.
  this.delete_player = delete_player;
  this.generate_draft_seating = generate_draft_seating;
  this.get_draft_seating_code = get_draft_seating_code;
  this.get_players_in_standings_order = get_players_in_standings_order;
  this.get_readable_lrn = get_readable_lrn;
  this.get_lrn = get_lrn;
  this.get_player_by_id = get_player_by_id;
  this.register_player = register_player;



  // Was there an object to load?
  if ( base !== null ) {

    // interesting fact: typeof null === "object"
    // see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof

    if (typeof base === "object" ) {

      // copy each property from the base object into the one we're creating
      for (let property in base) {
        if ( base.hasOwnProperty(property) ) {
          this[property] = base[property];
        }
      }

    } else {
      console.error("Sent the wrong type of argument (" + typeof base + ") to the Tournament object constructor.");
    }



  // If no object was sent, then create a brand-new tournament.
  } else {

    /* Default values */

    // Refers to the global 'version' variable.  If these ever get out of sync,
    // we'll know to warn the user that their tournament version is outdated.
    this.version = version;


    // Figure out a suitable default name for the tournament.
    // This feature is not actually implemented yet.
    let now = new Date;

    // If it's 10 or more minutes past the hour, round up to the next hour.
    // This is on the assumption that the tournament is being set up in advance.
    if ( now.getMinutes() >= 10 ) {
      now = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        now.getHours() + 1
      );
    }

    this.memorable_name = now.toLocaleString("en", {hour: "numeric"}) + " on "
                        + now.toLocaleString("en", {weekday: "long"}) + " "
                        + now.toLocaleString("en", {day: "numeric"}) + " "
                        + now.toLocaleString("en", {month: "long"});



    this.tb_floor = this.default_tb_floor;

    this.first_table_num = parseInt( $("#first_table").val() );
    if ( isNaN(this.first_table_num) ) {
      this.first_table_num = this.default_first_table_num;
      $("#first_table").val(this.default_first_table_num);
    }

    // this.games_to_win_a_match = this.default_games_to_win_a_match;

    this.games_to_win_a_match = parseInt( $("#games_to_win_a_match").val() );
    if ( isNaN(this.games_to_win_a_match) ) {
      this.games_to_win_a_match = this.default_games_to_win_a_match;
      $("#games_to_win_a_match").val(this.default_games_to_win_a_match);
    }

    this.points_for_a_draw = parseInt( $("#points_per_draw").val() );
    if ( isNaN(this.points_for_a_draw) ) {
      this.points_for_a_draw = this.default_points_for_a_draw;
      $("#points_per_draw").val(this.default_points_for_a_draw);
    }

    this.points_for_a_win = parseInt( $("#points_per_win").val() );
    if ( isNaN(this.points_for_a_win) ) {
      this.points_for_a_win = this.default_points_for_a_win;
      $("#points_per_win").val(this.default_points_for_a_win);
    }

    if ( $("#show_points_toggle").length > 0 ) {
      this.show_points_in_pairings = $("#show_points_toggle").prop("checked");
    }

    if ( $("#show_dropped_players_toggle").length > 0 ) {
      this.show_dropped_players_in_standings = $("#show_dropped_players_toggle").prop("checked");
    }

    this.draft_seating = [];  // An array of PIDs.
    this.rounds = [];  // An array of Round objects.

    this.players = [];  // An array of Player objects.
    this.register_player("Bye");

  }

}  // end of Tournament declaration



/**
 * Remove a player from the tournament.
 *
 * This is different from dropping a player.  If a player participated in at
 * least one round and then chose to leave (or was ejected), they should be
 * dropped.  If they were registered by mistake, or entered but then decided to
 * leave before play began, then this method is appropriate.
 *
 * Take note of a couple of unusual situations we have to account for when
 * deciding whether it's safe to delete a player:
 *
 *   - If a player has been paired for their first game but has yet to complete
 *     it, it's NOT safe to delete them.
 *
 *   - If a player joined the tournament late (e.g. during round 1), then
 *     decides they don't want to play after all and leaves, it IS appropriate
 *     to delete them.
 *
 * @param  {number} who The PID of the player to be deleted.
 * @return {void}
 */
function delete_player(who) {

  if ( this.get_player_by_id(who).ever_paired() ) {
    show_dialog("drop_not_delete");
  } else {
    this.players = _.reject(this.players, function(p){ return (p.pid === who); });
  }

}  // end of function delete_player()



/**
 * Shuffle the (non-bye) players and put their PIDs into the draft_seating
 * array.
 *
 * @return {array} The draft_seating array.
 */
function generate_draft_seating() {

  this.draft_seating =
    _.chain(this.players)
      .filter(function(p){ return p.pid !== 0; })
      .shuffle()
      .map(function(p){ return p.pid; })
      .value();

  return this.draft_seating;

}



/**
 * For each pid in the assigned seating list, find the corresponding player,and
 * put their name into a list item.
 *
 * @return {string} HTML string representing the draft_seating array as an <OL>.
 */
function get_draft_seating_code() {

  // Generate the seating order if it hasn't been done yet.
  if ( _.isEmpty(this.draft_seating) ) {
    this.generate_draft_seating();
  }

  return "<ol>" + _.map(
    this.draft_seating,
    function (p) {
      return "<li>"
           + _.find(
                tournament.players,  // have to use "tournament" here because "this" isn't what we want it to be
                function(player){ return player.pid == p; }
              ).name
           + "</li>";
    }
  ).join('') + "</ol>";

}



/**
 * tournament.rounds is zero-indexed, like any array, but humans think of the
 * first round as round 1, not round 0.  So this returns the number that
 * humans would think of as the latest round number (LRN).
 *
 * I have made this mistake many times during development.  I wrote this method
 * in the hope that it would help me avoid it in the future.
 *
 * @return {number} The latest round number, from a human's perspective.
 */
function get_readable_lrn() {
  return this.get_lrn() + 1;
}  // end function get_readable_lrn()



/**
 * Return the round number of the most recent round that exists, or, to put it
 * another way, the highest index of tournament.rounds
 *
 * "LRN" stands for "latest round number".
 *
 * This is intended for consumption by other functions; it is not what human
 * users think of as the latest round number.
 *
 * @return {number} The computer-readable number of the tournament's latest round.
 */
function get_lrn() {
  return this.rounds.length - 1;
}  // end function get_lrn()



/**
 * Search the list of Player objects in this tournament for the one with the
 * specified PID, and return it.
 *
 * @param  {number} player_id The PID of the player you're looking for.
 * @return {Player}           The Player object corresponding to that player.
 */
function get_player_by_id(player_id) {

  let found_player = _.find(this.players, function(p){ return (p.pid === player_id); });

  if ( typeof found_player === "undefined" ) {
    console.error("There is no player with ID " + player_id);
  }

  return found_player;

}  // end function get_player_by_id()



/**
 * Return the list of players in order by their current place in the standings.
 *
 * @param  {number}  as_of_round             The latest round to consider.
 * @param  {Boolean} include_the_bye         Whether or not to include the Bye player.
 * @param  {Boolean} include_dropped_players Whether or not to include players who have dropped before this round.
 * @return {array}                           An array of Player objects.
 */
function get_players_in_standings_order(as_of_round = null, include_the_bye = false, include_dropped_players = false) {

  // Establish defaults.
  if (   as_of_round === null
      || as_of_round >= tournament.rounds.length
  ) {
    as_of_round = this.get_lrn();
  }

  // Note that the sorts happen in reverse of the the tiebreakers' order of
  // importance, because... that's how sorting works.
  // This relies on underscore's sortBy being a stable sort (which it is,
  // according to https://underscorejs.org/#sortBy).
  return _.chain(this.players)
    .reject(function(p){ return !include_the_bye && p.pid === 0; })
    .reject(function(p){ return !include_dropped_players && !_.isNull(p.dropped_after_round) && p.dropped_after_round < as_of_round; })
    .sortBy(function(p){ return p.calculate_ogw(as_of_round); })
    .sortBy(function(p){ return p.calculate_gwp(as_of_round); })
    .sortBy(function(p){ return p.calculate_omw(as_of_round); })
    .sortBy(function(p){ return p.get_match_points(as_of_round); })
    .value()
    .reverse();  // so that the highest-ranked player will be at the top of the list

}  // end function get_players_in_standings_order()



/**
 * Add someone to the list of players participating in this tournament.
 *
 * Duplicate name checking is handled by add_player().  This function doesn't
 * make a redundant check for duplicate names, but maybe it should.
 *
 * @param  {string} name [description]
 * @return {void}
 */
function register_player(name) {

  // Find the highest Player ID yet assigned, and assign this new user a Player
  // ID that's one higher.
  //
  // We start with -1 as the "memo" because we're adding 1 after the reduce,
  // and we want the "Bye" player to have PID 0.
  let id_num = _.reduce(
    this.players,
    function(memo, p){ return p.pid > memo ? p.pid : memo; },
    -1
  ) + 1;

  this.players.push( new Player(name, id_num) );

}
