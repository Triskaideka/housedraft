/**
 * enhanced_mode.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * Adds certain improvements to the site when it is run from the
 * housedraft.games server, instead of from a local disk or any other web site.
 *
 * Also recognizes housedraft.local as an "enhanced" server, for development
 * purposes.
 *
 * If you clone this project and set it up on your own server, you might want
 * to adapt the line in this file that sets the value of enhanced_mode so that
 * it recognizes your server's name as well.  (Of course, you don't *have* to
 * turn on enhanced mode if you don't want to.)
 *
 * The main thing you need for the enhancements to work is for the server to
 * run what's in the .htaccess file.  That probably means that it needs to use
 * the Apache httpd, and Apache needs to be configured to allow .htaccess files
 * to do all of the things that this project's does.  If you're having trouble
 * getting the .htaccess file to work, check the official documentation:
 *
 *     http://httpd.apache.org/docs/current/howto/htaccess.html
 */

var enhanced_mode = false;

// Run this code on page load...
$(document).ready(function(){

  let this_site_regex = new RegExp(/housedraft\.(games|local)$/);

  // ...only if this page's address indicates that it was opened from the
  // housedraft.games server.
  enhanced_mode = ( this_site_regex.test(window.location.hostname) );
  if ( ! enhanced_mode ) { return; }



  // Iterate over each link on the page.
  $("a").each(function(){

    let target_url = new URL(this);

    // Don't modify links that point to other sites.
    if ( ! this_site_regex.test(target_url.hostname) ) {
      return;
    }

    // Strip trailing "index.html" or just ".html".
    target_url.pathname = target_url.pathname.replace(/index\.html$/, '');
    target_url.pathname = target_url.pathname.replace(/\.html$/, '');

    // Don't let a link be empty.
    if (target_url.pathname.length < 1) {
      target_url.pathname = './';
    }

    // Update the link's href attribute.
    $(this).attr('href', target_url.href);

  });  // end of $("a").each()

});  // end of $(document).ready()
