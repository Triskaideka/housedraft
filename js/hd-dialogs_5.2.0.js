/**
 * hd-dialogs.js
 *
 * Store data and code for dialogs in HouseDraft.
 *   https://housedraft.games/
 *
 * This file should be included last or close to last, because the "dialogs"
 * variable references function names that need to be defined by now.
 */

/**
 * preset_dialogs is a JSON object containing a selection of dialogs that the
 * site might use.  You can call show_dialog() with the name of one of these
 * dialogs; however, if you need to insert a variable into the content of your
 * dialog, then you should instead call it with an object (having the same
 * structure as indicated here).
 *
 * It would be nice to store this as a separate .json file, but I want the
 * site to be able to work when opened from a local filesystem (i.e. not from
 * a remote server).
 *
 * The structure of the information to send for each dialog is as follows:
 *
 *  "dialog_name": {
 *    "title": "Top line of dialog",
 *    "message": "Body text of the dialog",
 *    "buttons": [
 *      {"label": "Text of button 1", "function": function_to_call},
 *      {"label": "Text of button 2", "function": function_to_call}
 *    ],
 *    "return_focus_to": "selector"
 *  },
 *
 * The "dialog_name" part is what you'll send to show_dialog() so it knows
 * which dialog to show.
 *
 * The "message" will be wrapped in a paragraph tag, but if you want a paragraph
 * break in the middle of it, you can put a "</p><p>" in it.
 *
 * You can have any number of buttons in the "buttons" key.  You can also
 * omit this key, and show_dialog will use a generic OK button.
 *
 * If you want a "Cancel" button that does nothing other than getting rid of
 * the dialog, omit the "function" key.
 *
 * Note that the value of "function" is an actual function, not a string
 * containing the name of a function.
 *
 * "return_focus_to" should be a string containing a selector that jQuery will
 * understand.  The selected element will be focused after the dialog box is
 * dismissed.
 */
var preset_dialogs = {

  "_unimplemented": {
    "title": "Unimplemented dialog",
    "message": "Sorry; this is a programming error.  A dialog window was opened, but we don't know what for."
  },

  "clear_tournament": {
    "title": "Restart tournament?",
    "message": "Are you sure? This will delete the records of the current tournament and there is no way to get them back.",
    "buttons": [
      {
        "label": "No, this tournament is still in progress"
      },
      {
        "label": "Yes, delete the tournament",
        "function": clear_tournament
      }
    ]
  },

  "drop_not_delete": {
    "title": "Drop, don't delete",
    "message": "You can't delete a player after they've participated in a match.  You want to drop them instead.  Open the \"Players\" section and click the \"drop\" command next to their name."
  },

  "games_to_win_change": {
    "title": "\"Games to win\" setting can't be changed right now",
    "message": "The number of games it takes to win a match can't change in the middle of a tournament.  You may need to reset it after the next time you start a new tournament.  Other settings have been restored to their defaults."
  },

  "no_way_to_pair_unpaired": {
    "title": "Couldn't pair the unpaired players",
    "message": "After examining the list of players who have not dropped and are not already paired in this round, it turns out there's no legal way to pair them.  (The usual rules about pairing still have to be followed, e.g., players who have already faced one another can't be paired again.)</p><p>If you really want to try to include these players, and the round has only just begun, consider deleting the round and re-starting it.  If the round is well underway, just have the late arrivals sit out for now; they'll be paired in the next round."
  },

  "overwrite_draft_seating": {
    "title": "Change the draft seating?",
    "message": "Draft seating has already been generated.  Are you sure you want to re-generate it?  You won't be able to retrieve the previous seating arrangement.",
    "buttons": [
      {
        "label": "Keep the current seating arrangement"
      },
      {
        "label": "Generate a new seating order",
        "function": assign_seating_for_draft
      }
    ]
  },

  "unable_to_pair": {
    "title": "Unable to pair another round",
    "message": "Could not find a suitable set of pairings! This probably means the tournament has gone on too long."
  }

};



/**
 * The generic "OK" button for when you don't include any specific buttons
 * in your dialog.
 * @type {Array}
 */
var ok_button = [ {"label": "OK", "function": dismiss_dialog} ];



/**
 * Call this to get rid of the currently open dialog box (for whatever reason).
 * @return {void}
 */
function dismiss_dialog() {

  $("#overlay_back").hide();

  /* According to https://api.jquery.com/empty/ , "jQuery removes other
  constructs such as data and event handlers from the child elements before
  removing the elements themselves."  We do want this, but if we didn't, we
  could use .detach() instead. */
  $("#dialog_box").empty();

  // Return the focus to the element that was specified when show_dialog() was
  // called (if any).
  //
  // #dialog_box was emptied but not destroyed, so its attributes should still
  // be intact, so we can still read "data-return_focus_to" here.
  if ( $("#dialog_box").data("return_focus_to") ) {
    $( $("#dialog_box").data("return_focus_to") ).focus();
  }

}



/**
 * Open a dialog box, showing a predetermined message and set of buttons.
 * @param  {String|Object} dialog_info The dialog contents to use: either a string referring to a key of preset_dialogs, or an object containing all the dialog info itself.
 * @return {void}
 */
function show_dialog(dialog_info = "_unimplemented") {

  let box_top_position = 0
    , code = ''
    ;

  // If we got a reference to a preset dialog, load it in.
  if ( typeof(dialog_info) == "string" ) {
    if ( preset_dialogs.hasOwnProperty(dialog_info) ) {
      dialog_info = preset_dialogs[dialog_info];
    } else {
      dialog_info = preset_dialogs["_unimplemented"];
    }
  }

  if ( ! dialog_info.hasOwnProperty("buttons") ) {
    dialog_info.buttons = ok_button;
  }

  code = `<h2>${dialog_info.title}</h2>
          <div><p>${dialog_info.message}</p></div>
          <div class="dialog_buttons">`;

  // Write the dialog's buttons.
  for (button of dialog_info.buttons) {
    code += `<button>${button.label}</button>`;
  }

  code += "</div>";

  $("#dialog_box").html(code);

  // Make the buttons work.
  for (let i = 0; i < dialog_info.buttons.length; i++) {
    if ( dialog_info.buttons[i].hasOwnProperty("function") ) {

      $("#dialog_box .dialog_buttons button:nth-of-type("+(i+1)+")")
        .click(dialog_info.buttons[i].function);

    }
  }



  // Regardless of what any button does, all of them should dismiss the
  // dialog when clicked.
  $("#dialog_box .dialog_buttons button").click(dismiss_dialog);



  // Make a note of which element focus should be returned to afterwards.
  // dismiss_dialog() is responsible for following through on this.
  if ( dialog_info.hasOwnProperty("return_focus_to") ) {
    $("#dialog_box").data("return_focus_to", dialog_info["return_focus_to"]);
  }



  // Now that the dialog's set up, change the styles to display it.
  $("#overlay_back").show();  // make the overlay & dialog box visible

  // Vertically center the box.
  box_top_position = (( $(window).height() - $("#dialog_box").height() ) / 2);
  if (box_top_position < 0) { box_top_position = 0; }
  $("#dialog_box").css("top", box_top_position);

  $("#dialog_box button:nth-of-type(1)").focus();  // focus on the first button

}
