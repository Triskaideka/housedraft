/**
 * hd-lib.js
 *
 * JavaScript library for HouseDraft
 *   https://housedraft.games/
 */

// Global variables

// The version number of the software, to be stored in the tournament object for comparison purposes.
// As a rule, increase the "major" portion when and only when a change has been made that renders previous
// tournament objects incompatible with the site.  The "minor" and "bugfix" numbers don't affect anything
// and can be changed at will.
var version = { "major": 5, "minor": 2, "bugfix": 0 }

var announcement_rotation_interval
  , clock_interval

  // The total time, in milliseconds, for which the current round was intended to last when it was started.
  , length_of_round = 0

  , one_second = 1000  // the number of milliseconds in a second

  // The time, in milliseconds, remaining in the current round.
  // If it's set to NaN then that means there's no round in progress.
  , time_left_in_round = NaN

  , tournament = new Tournament
  ;



// The announcements that can go in the announcement area if the user doesn't set one.
var random_announcements = [
  "I came here to bolt birds and chew bubblegum... and I'm all out of gum.",
  "Don't forget to hold back a land!",
  "Remember, the only life point that matters is the last one!",
  "If the only way for you to win involves drawing one particular card, play as if you are going to draw that card.",
  "Always give your opponent the chance to make a mistake.",
  "Submit bug reports or feature requests for HouseDraft at:\n\nhttps://gitlab.com/Triskaideka/housedraft/-/issues",
];



/**
 * Register a new player in the tournament.  Reject the new player if there's
 * already a player with the same name.
 *
 * @return {void}
 */
function add_player() {

  let name = _.escape( $("#new_player_name").val() );

  // Don't do anything if there's nothing in the new_player_name field;
  // it was probably a misclick.
  if ( ! name ) { return; }

  // Prevent duplicate player names.  Internally this tool uses player IDs to
  // tell players apart, but the user doesn't see those, so if we were to
  // allow duplicate player names, there would be no way for users/players to
  // know which of the repeated names in the pairings or standings belonged
  // to whom.
  if ( _.findIndex(tournament.players, function(p){ return p.name === name; }) > -1 ) {

    show_dialog({
      "title": "Duplicate player name",
      "message": `
        There's already a player named
        <strong class="prevent_long_words">${name}</strong>.
        Please use a unique name for each player.</p>
        <p>If you really have two players by the same name, it might help to
        use middle or last initials, or to call one of them by a nickname.
      `,
      "return_focus_to": "#new_player_name"
    });

  // If there was a real, non-duplicate name for the player,
  // register them.
  } else {

    tournament.register_player(name);

    // Now that there's at least one player, it's okay to start the tournament
    // (if we haven't already).
    if ( tournament.rounds.length < 1 ) {
      $("#player_list .button_set button").prop("disabled", false);
    }

    update_player_list(tournament.players);
    show_or_hide_pair_unpaired_players_control();

    $("#new_player_name").val("");  // empty the name field now that we've used it
    $("#new_player_name").focus();  // assume the user wants to keep entering more players, and put the cursor back

    save();

  }

}  // end function add_player()



/**
 * Treat the current round as having ended.
 * Calculate its standings and show them.
 * But do not create the next round yet.
 *
 * @return {void}
 */
function advance_to_standings() {

  // Error checking.
  if ( $(".round[data-round-num=" + tournament.get_lrn() + "]").length < 1 ) {
    console.error(
      "Can't advance to the standings for round " + round_num +
      " because no page section for that round exists yet."
    );
    return;
  }

  // Hide all sections in the player area except for the one we're about to create/open.
  close_sections("#player_list, .round:not(:last-of-type), .round .pairings, .round .standings");

  // Create a standings section for the round, if it doesn't already exist.
  if ( $(".round[data-round-num=" + tournament.get_lrn() + "] .standings").length < 1 ) {
    $(".round[data-round-num=" + tournament.get_lrn() + "]").append( get_standings_section( tournament.get_lrn() ) );
  }

  // Whether or not the appropriate standings section was just created, make sure it's open.
  open_sections(".round[data-round-num=" + tournament.get_lrn() + "] .standings");

}  // end function advance_to_standings()



/**
 * Call this when the "Starting table number" setting is altered.
 *
 * The change will be reflected when the next round begins.  Table numbers
 * for old rounds don't get changed retroactively (they were correct at the
 * time, presumably).  The table number is actually stored in the Round object.
 *
 * @return {void}
 */
function apply_first_table_setting() {

  let new_val = parseInt( $("#first_table").val() );

  if ( !isNaN(new_val) ) {
    tournament.first_table_num = new_val;
  }

  save();

}



/**
 * Call this when the "Matches end after one player wins X games" setting is
 * altered, to update the page appropriately.
 *
 * @return {void}
 */
function apply_games_to_win_setting() {

  let new_val = parseInt( $("#games_to_win_a_match").val() );

  if ( !isNaN(new_val) ) {

    // I don't think we should permit changing the number of games per match in
    // the middle of a tournament.
    if (tournament.rounds.length < 1) {

      tournament.games_to_win_a_match = new_val;
      save();

    } else {

      show_dialog("games_to_win_change");

    }

  }

}



/**
 * Call this when the "Points for a draw" setting is altered,
 * to update the page appropriately.
 *
 * @return {void}
 */
function apply_points_per_draw_setting() {

  // We use "Number" instead of "parseInt" to convert the string,
  // because the number of points awarded doesn't have to be an integer.
  // (For example, chess usually awards 0.5 points for a draw.)
  let new_val = Number( $("#points_per_draw").val() );

  if ( !isNaN(new_val) ) {
    tournament.points_for_a_draw = new_val;
  }

  save();
  update_standings_tables();

}



/**
 * Call this when the "Points for a win" setting is altered,
 * to update the page appropriately.
 *
 * @return {void}
 */
function apply_points_per_win_setting() {

  // We use "Number" instead of "parseInt" to convert the string,
  // because the number of points awarded doesn't have to be an integer.
  // (For example, chess usually awards 0.5 points for a draw.)
  let new_val = Number( $("#points_per_win").val() );

  if ( !isNaN(new_val) ) {
    tournament.points_for_a_win = new_val;
  }

  save();
  update_standings_tables();

}



/**
 * Call this when the "Show dropped players in standings" setting is altered,
 * to update the page appropriately.
 *
 * @return {void}
 */
function apply_show_dropped_players_setting() {

  if ( $("#show_dropped_players_toggle").prop("checked") ) {

    $("main").removeClass("hide_dropped_players_from_standings");
    tournament.show_dropped_players_in_standings = true;

  } else {

    $("main").addClass("hide_dropped_players_from_standings");
    tournament.show_dropped_players_in_standings = false;

  }

  save();

}



/**
 * Call this when the "Show points in pairings" setting is altered, to
 * update the page appropriately.
 *
 * @return {void}
 */
function apply_show_points_setting() {

  if ( $("#show_points_toggle").prop("checked") ) {

    $("main").addClass("show_points");
    tournament.show_points_in_pairings = true;

  } else {

    $("main").removeClass("show_points");
    tournament.show_points_in_pairings = false;

  }

  save();

}



/**
 * Generate randomized seating for the draft, or re-generate it if it already
 * existed.
 *
 * Call confirm_assign_seating() and let it call this function, so the user is
 * aware if they're overwriting a previously generated seating order.
 *
 * @return {void}
 */
function assign_seating_for_draft() {

  tournament.generate_draft_seating();
  display_draft_seating();

}



/**
 * Call this when the page is loaded or resized.
 *
 * @return {void}
 */
function autosize_announcements() {

  // Set the height of the message region so it takes up all of the remaining
  // space.
  message_height = $(window).innerHeight() - $("#message").offset().top;
  $("#message").outerHeight( message_height );

  // Increase the height of the announcements textarea row by row until it's too
  // big.
  $("#message textarea:first").attr("rows", 2);
  while ( $("#message textarea:first").offset().top + $("#message textarea:first").height() < $(window).height() ) {
    $("#message textarea:first").attr("rows", parseInt( $("#message textarea:first").attr("rows") ) + 1);
  }

  // Adjust the textarea so it's not too long or too short.
  $("#message textarea:first").attr("rows", parseInt( $("#message textarea:first").attr("rows") ) - 2);
  if ( parseInt( $("#message textarea:first").attr("rows") ) < 3 ) {
    $("#message textarea:first").attr("rows", 3);
  }

}  // end of function autosize_announcements()



/**
 * Restart with a fresh tournament.
 *
 * @return {void}
 */
function clear_tournament() {

  tournament = new Tournament;

  update_player_list([]);

  // Reset the assigned seating button to its original wording.
  $("button#assign_seating").text( $("button#assign_seating").data("original-wording") );

  $(".round").remove();
  $("*[data-round-num]").remove();
  $("#seating ol").remove();
  $("#seating").hide();
  $("main").removeClass("underway");

  enable_or_disable_games_to_win_control();
  enable_or_disable_starting_controls();

  open_sections("#player_list");

  save();

}  // end of function clear_tournament()



/**
 * Specifically for closing one or more of the sections in the #players region
 * of the page.
 *
 * See also open_sections().
 *
 * @param  {string} selector A CSS selector indicating which section(s) to close.  Remember that you can use commas to close multiple sections.
 * @return {void}
 */
function close_sections(selector) {
  $(selector).slideUp();
  $(selector).prev("h2,h3").addClass("collapsed");
  $(selector).prev("h2,h3").removeClass("expanded");
}



/**
 * Gather whatever results data is in the pairings table and store it in each
 * round's matches array.
 *
 * @return {void}
 */
function collect_results() {

  for (let r = 0; r < tournament.rounds.length; r++) {
    for (let match of tournament.rounds[r].matches) {

      let selector_prefix = ".round[data-round-num='" + r + "'] input[type='number']";

      // Record the number of draws.
      if ( $(selector_prefix + "[data-draw-pids*='(" + match.players[0].pid + ")']").length ) {
        match.draws =
          parseInt(
            $(selector_prefix + "[data-draw-pids*='(" + match.players[0].pid + ")']").val()
          ) || 0;
      }

      // Record the left player's wins.
      if ( $(selector_prefix + "[data-win-pid='" + match.players[0].pid + "']").length ) {
        match.players[0].wins =
          parseInt(
            $(selector_prefix + "[data-win-pid='" + match.players[0].pid + "']").val()
          ) || 0;
      }

      // Record the right player's wins.
      if ( $(selector_prefix + "[data-win-pid='" + match.players[1].pid + "']").length ) {
        match.players[1].wins =
          parseInt(
            $(selector_prefix + "[data-win-pid='" + match.players[1].pid + "']").val()
          ) || 0;
      }

    }
  }

  save();
  update_standings_tables();

}



/**
 * Before advancing from the pairings to the standings, we do a quick check to
 * see if there are any matches with no results recorded, and warn the user if
 * so.  We'll do another check, with different criteria, before advancing from
 * standings to the next round:  see confirm_create_new_round().
 *
 * @return {void}
 */
function confirm_advance_to_standings() {

  if ( tournament.rounds[tournament.get_lrn()].all_matches_have_results() ) {

    advance_to_standings();

  } else {

    show_dialog({
      "title": `There are unfinished matches in round ${ tournament.get_lrn() }`,
      "message": `
        Some of the matches in this round don't have results yet.  Are you sure you want to calculate the standings
        anyway?</p><p>If there's a pair of players that started a game but didn't finish it before time ran out, you
        should record that as one drawn game.  If there are players that have left the tournament and don't
        plan to return, you should mark them as having dropped.</p><p>However, if there are a pair of players that did
        not even start their match this round, but they still intend to play in the next round, then it may be correct
        to record a 0-0-0 result.</p><p>Your decision here isn't final; you'll be able to return and edit the match
        results later.
      `,
      "buttons": [
        {"label": `No, I still need to record some results`},
        {"label": `Yes, calculate the standings based on these match results`, "function": advance_to_standings}
      ]
    });

  }

}  // end function confirm_advance_to_standings()



/**
 * Before generating draft seating, check whether it already exists, and if it
 * does, ask the user to confirm overwriting it.
 *
 * @return {void}
 */
function confirm_assign_seating() {

  if ( _.isEmpty( tournament.draft_seating ) ) {
    assign_seating_for_draft();
  } else {
    show_dialog("overwrite_draft_seating");
  }

}  // end function confirm_assign_seating()



/**
 * Before creating a new round, double-check whether there's a round in progress
 * that appears not to have had all its results recorded.
 *
 * The situation we check for here is more dire than the one we check for in
 * confirm_advance_to_standings().
 *
 * @return {void}
 */
function confirm_create_new_round() {

  // We can pass on through if (a) we're about to pair the first round, or
  // (b) at least one result has been recorded for the current round.
  if (   tournament.rounds.length < 1
      || tournament.rounds[tournament.get_lrn()].has_any_results()
  ) {

    create_new_round();

  } else {

    let r = tournament.get_readable_lrn();

    show_dialog({
      "title": `There are no results in round ${r}`,
      "message": `
        You haven't recorded <i>any</i> results for round ${r}.  Are you sure you want to move on to the next round?
        All matches in this round will be considered draws, and it won't be possible for any of the player pairings in
        this round to be repeated in future rounds.
      `,
      "buttons": [
        {"label": `No, I still need to record some results`},
        {"label": `Yes, proceed to the next round even though all matches in this one are draws`, "function": create_new_round}
      ]
    });

  }

}  // end function confirm_create_new_round()



/**
 * Show a dialog box asking the user whether they really want to re-pair the
 * round.
 *
 * Even if there are no results for the round, there may be matches already in
 * progress, so we should still make sure the user understands what they're
 * doing.
 *
 * @return {void}
 */
function confirm_delete_latest_round() {

  let incorrect_result_link = "instructions.html#incorrect_result"
    , r = tournament.get_readable_lrn()
    ;

  if (enhanced_mode) {
    incorrect_result_link = "instructions#incorrect_result";
  }

  show_dialog({
    "title": `You're about to delete round ${r}'s data!`,
    "message": `
      You asked to delete round <strong>${r}</strong>.</p>
      <p>Please read <a target="_blank" href="${incorrect_result_link}">"A match result was
      entered incorrectly"</a> on the instructions page for some guidance on when to delete a
      round, and when it's best to let an incorrect result stand.</p>
      <p>If you already entered any match results for round ${r}, they will be deleted.
      You may want to make a note of them first, so you can re-enter them if they were accurate.</p>
      <p>Do you still want to delete round ${r}?
    `,
    "buttons": [
      {"label": `No, leave the round as it is`},
      {"label": `Yes, delete all pairings &amp; results for round ${r}`, "function": delete_latest_round}
    ]
  });

}  // end function confirm_delete_latest_round()



/**
 * Show a dialog asking the user if they're sure they want to pair the
 * currently unpaired players.
 *
 * Also explains why they might *not* want to.
 *
 * @return {void}
 */
function confirm_pair_unpaired() {

  let r = tournament.get_readable_lrn();

  let bye_notice = ( tournament.rounds[ tournament.get_lrn() ].anybody_has_a_bye()
    ? `</p><p>Note: the player who currently has a bye may be included in the new pairings.`
    : ''
  );

  show_dialog({
    "title": `Are you sure you want to pair the unpaired players?`,
    "message": `
      Are you sure that you want to add the currently unpaired players to round ${r}?</p>
      <p>You don't have to&mdash;you can leave them out of this round, and
      they'll be paired in the next round.  Players who remain unpaired in this
      round won't earn match points for this round, and it won't affect their
      tiebreakers.</p>
      <p>If you do pair them now, you should give them the full amount of time
      to play their match for this round.  Don't extend the clock for the
      other players, just for the new match(es).</p>
      <p>Rule of thumb: if it's still early in the round, pair them.  If it's
      late in the round, make them wait.</p>
      ${bye_notice}
    `,
    "buttons": [
      {"label": `No, leave these players out until the next round`},
      {"label": `Yes, pair the unpaired players`, "function": pair_unpaired_and_update}
    ]
  });

}  // end function confirm_pair_unpaired()



/**
 * Create a new round, fill out its pairing data, update the display.
 *
 * @return {void}
 */
function create_new_round() {

  // Create the new round.
  tournament.rounds.push(new Round);

  // This function doesn't directly do anything with the return value,
  // except test whether it's empty.
  let matches = tournament.rounds[tournament.get_lrn()].get_matches();



  // But if we failed to pair the new round, don't just leave it there un-filled.
  if ( _.isEmpty(matches) ) {

    tournament.rounds.pop();

  // If we successfully paired the round, save it and update the page display.
  } else {

    // Save state now, in case the computer crashes or the window is reloaded.
    save();

    // Close the sections for previous rounds, and update them to reflect that
    // they are over.
    close_sections("#player_list, .standings, .round");
    $("#player_list .button_set button").prop("disabled", true);  // The buttons for starting the tournament.
    $("#players h2[data-round-num], .round, .round h3").addClass("completed");

    // Update the page display to show the new round.
    create_round_section( tournament.get_lrn() );
    $("main").addClass("underway");
    $("#delete_latest_round").prop("disabled", false);
    open_sections(".round[data-round-num="+tournament.get_lrn()+"]");

    // Put the cursor inside the first input field in the new round's pairings section.
    $(".round[data-round-num="+tournament.get_lrn()+"] .pairings input[type=\"number\"]:first").focus();

  }



  // Disallow changing the number of games in a match after round 1.
  enable_or_disable_games_to_win_control();

}  // end of function create_new_round()



/**
 * Build the HTML code for a new section of the page dedicated to the indicated
 * round.  Append it to the #players section.
 *
 * If this round is the latest round, then the standings table won't be
 * generated yet.
 *
 * @param  {number}  round_num  The round whose data should be used.
 * @return {void}
 */
function create_round_section(round_num) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return;
  }

  let code = ''
    , is_latest_round = ( round_num === tournament.get_lrn() )
    , readable_round_num = round_num + 1
    ;

  // Section heading.
  code = `
    <h2 class="${is_latest_round ? 'expanded' : 'completed collapsed'}" data-round-num="${round_num}">
      <a href="#">Round ${readable_round_num}</a>
    </h2>
    <div class="${is_latest_round ? '' : 'hidden completed'} round" data-round-num="${round_num}">
  `;

  // Pairings subsection.
  // Note that this section is always expanded when it's created.  If this
  // isn't the latest round, it's the .round section that will be collapsed.
  code += `
    <h3 class="expanded ${is_latest_round ? '' : 'completed'}"><a href="#">Pairings</a></h3>
    <div class="pairings listing">
    <p class="completed_round_notice">Round ${readable_round_num} has ended,
    but you can still edit its results if you need to correct any errors.</p>
    <table>
    <caption>Pairings for round ${readable_round_num}</caption>
    <thead><tr>
    <th scope="col">Table</th>
    <th scope="col">Player</th>
    <th scope="col">Wins</th>
    <th scope="col">Draws</th>
    <th scope="col">Wins</th>
    <th scope="col">Player</th>
    </tr></thead>
    ${tournament.rounds[round_num].get_pairings_tbody()}
    </table>
  `;

  // For rounds that aren't the latest round, this button will be hidden,
  // but it still needs to be there, in case later rounds are deleted and
  // this round becomes the latest round.
  code += `
    <p class="button_set">
    <button class="advance_to_standings">Done entering results for round ${readable_round_num}</button>
    </p>
  `;

  // Close the class="pairings listing" div.
  code += '</div>';

  // Standings subsection.
  if (!is_latest_round) {
    code += get_standings_section(round_num);
  }

  // Close the class="round" div.
  code += '</div>';

  // Add this section's code to the page.
  $("#players").append(code);

}  // end function create_round_section()



/**
 * Delete the data on the current round and re-pair it.
 *
 * This is destructive; you should probably call confirm_delete_latest_round()
 * first.
 *
 * @return {void}
 */
function delete_latest_round() {

  // Remove the round's section from the page.
  $("h2[data-round-num="+tournament.get_lrn()+"]").remove();
  $(".round[data-round-num="+tournament.get_lrn()+"]").remove();

  // Remove the round's data from the tournament.
  tournament.rounds.pop();
  save();

  // If there's still at least one round on the books:
  if ( tournament.rounds.length > 0 ) {

    // Remove the "completed" class from the new latest round.
    $("#players h2[data-round-num="+tournament.get_lrn()+"]").removeClass("completed");
    $(".round[data-round-num="+tournament.get_lrn()+"]").removeClass("completed");
    $(".round[data-round-num="+tournament.get_lrn()+"] h3").removeClass("completed");

    // Open the section for the new latest round.
    open_sections(".round[data-round-num="+tournament.get_lrn()+"] .standings");
    open_sections(".round[data-round-num="+tournament.get_lrn()+"]");

  // If we've rewound to the beginning of the tournament:
  } else {

    $("main").removeClass("underway");
    $("#delete_latest_round").prop("disabled", true);

    if (tournament.players.length <= 1) {  // <= because "Bye" is a player
      $("#player_list .button_set button").prop("disabled", true);
    } else {
      $("#player_list .button_set button").prop("disabled", false);
    }

    open_sections("#player_list");

  }

  // Changing the number of games in a match shouldn't be allowed after round 1.
  enable_or_disable_games_to_win_control();

}  // end function delete_latest_round()



/**
 * Delete the indicated player from the player list.
 *
 * This is different from dropping a player.  See the notes on Player.drop()
 * and Tournament.delete_player().
 *
 * See also add_player() and drop_player().
 *
 * @param  {number} who The PID of the player to be removed.
 * @return {void}
 */
function deregister_player(who) {

  who = parseInt(who);  // make sure it's an integer

  tournament.delete_player(who);

  update_player_list(tournament.players);
  show_or_hide_pair_unpaired_players_control();

  if (tournament.players.length <= 1) {  // <= because "Bye" is a player
    // Disable all buttons in the player list section, under the assumption that
    // all of those functions require at least one player to be registered
    // (will need to change this selector if that becomes untrue in the future).
    $("#player_list .button_set button").prop("disabled", true);
  }

  save();

}  // end function deregister_player()



/**
 * Display the contents of tournament.draft_seating as a list in the
 * #player_list section.
 *
 * This function calls tournament.get_draft_seating_code(), which will generate
 * the seating order if it does not already exist.  If the seating order *does*
 * already exist and you need to re-regenerate it, call
 * assign_seating_for_draft().
 *
 * @return {void}
 */
function display_draft_seating() {

  // If there was a previous seating list, get rid of it.
  $("#seating ol").remove();

  $("#seating").append( tournament.get_draft_seating_code() );

  // .get_draft_seating_code() may have created a new seating order,
  // so we need to save it.
  save();

  $("#player_list button#assign_seating").text(
    $("#player_list button#assign_seating").data("revised-wording")
  );

  $("#seating").slideDown();

}  // end function display_draft_seating()



/**
 * Mark a player as having dropped, but keep them in the player list.
 *
 * See also deregister_player() and reinstate_player().
 *
 * @param  {number} who         The PID of the player to bedroped.
 * @param  {number} after_round The number of the last round that they participated in.
 * @return {void}
 */
function drop_player(who, after_round = null) {

  // Validate the arguments.
  who = parseInt(who);  // Make sure it's an integer.

  if (   after_round === null
      || after_round >= tournament.rounds.length
  ) {
    after_round = tournament.get_lrn();
  }

  // Error checking.
  if (who === 0) {
    console.warn("It shouldn't be possible to drop the Bye player; this is a bug.");
    return;
  }

  // Drop the player.
  tournament.get_player_by_id(who).drop(after_round);

  // Update the display.
  update_player_list(tournament.players);
  show_or_hide_pair_unpaired_players_control();
  update_standings_tables();

  save();

}  // end function drop_player()



/**
 * Call this when the number of rounds changes (or may have changed) to
 * determine whether the "Matches end after one player wins N games" control
 * should be disabled or not.
 *
 * @return {void}
 */
function enable_or_disable_games_to_win_control() {

  let status = (
    typeof tournament !== "undefined"
      &&
    tournament.rounds.length > 1
  );

  $("#games_to_win_a_match").prop("disabled", status);
  $("#games_to_win_a_match").prev(".number_field_decrement").prop("disabled", status);
  $("#games_to_win_a_match").next(".number_field_increment").prop("disabled", status);

}



/**
 * If there's at least one player registered, and round 1 hasn't started,
 * enable the "Assign seating for draft" and "Begin round 1" buttons.
 *
 * (Not sure why you'd want to start the tournament with only one player,
 * but you can.)
 *
 * @return {void}
 */
function enable_or_disable_starting_controls() {

  if (   tournament.players.length > 1  // >1 because "Bye" is a player
      && tournament.rounds.length < 1
  ) {
    $("#player_list #begin_tournament button").prop("disabled", false);
  } else {
    $("#player_list #begin_tournament button").prop("disabled", true);
  }

}



/**
 * Get the HTML code for a standings sub-section within a round section.
 *
 * This basically wraps the rest of the section around the results of
 * get_standings_table().
 *
 * Note that this section is always expanded when it's created.  If this
 * isn't the latest round, it's the .round section that will be collapsed.
 *
 * @param  {number} round_num The number of the round to get standings for.
 * @return {string}           A string of HTML code containing the entire standings section.
 */
function get_standings_section(round_num) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return '';
  }

  let code = ''
    , is_latest_round = ( round_num === tournament.get_lrn() )
    , readable_round_num = round_num + 1

    // Add 1 because it's the next round and another 1 to make it make sense to the human users.
    , readable_next_round_num = round_num + 2
    ;

  code += `
    <h3 class="expanded ${is_latest_round ? '' : 'completed'}"><a href="#">Standings</a></h3>
    <div class="standings listing">
      ${get_standings_table(round_num)}
  `;

  // For rounds that aren't the latest round, these buttons will be hidden,
  // but they still need to be there, in case later rounds are deleted and
  // this round becomes the latest round.
  code += `
    <p class="button_set">
      <button class="return_to_pairings">Go back &amp; edit round ${readable_round_num}'s pairings</button>
      <button class="advance_to_next_round">Advance to round ${readable_next_round_num}</button>
    </p>
  `;

  code += `</div>`;

  return code;

}  // end function get_standings_section()



/**
 * Get the code for the standings table for the indicated round.
 *
 * @param  {number} as_of_round The round number that you want the standings table to go up to.
 * @return {string} HTML code for the table.
 */
function get_standings_table(as_of_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return '';
  }

  // Establish defaults.
  if (as_of_round === null) {
    as_of_round = tournament.get_lrn();
  }



  // Variables.
  let place_num = 1
    , readable_round_num = as_of_round + 1
    ;

  let code = `<table>
    <caption>Standings after round ${readable_round_num}</caption>
    <thead><tr>
    <th scope="col">Place</th>
    <th scope="col">Player</th>
    <th scope="col">Match points</th>
    <th scope="col"><abbr title="Opponents' Match Win Percentage">OMW%</abbr></th>
    <th scope="col"><abbr title="Game Win Percentage">GW%</abbr></th>
    <th scope="col"><abbr title="Opponents' Game Win Percentage">OGW%</abbr></th>
    </tr></thead>
    <tbody>`;

  let players = tournament.get_players_in_standings_order(as_of_round, false, true);




  // Add a row for each player.
  for (let i = 0; i < players.length; i++) {

    p = players[i];

    // Increase the place number, UNLESS this player is
    // tied on all tiebreakers with the previous player.
    if (   i > 0  // don't need to compare the top-placed player with anyone
        && p.get_match_points(as_of_round) == players[i-1].get_match_points(as_of_round)
        && p.calculate_omw(as_of_round)    == players[i-1].calculate_omw(as_of_round)
        && p.calculate_gwp(as_of_round)    == players[i-1].calculate_gwp(as_of_round)
        && p.calculate_ogw(as_of_round)    == players[i-1].calculate_ogw(as_of_round)
    ) {
      // Do nothing; the players are tied and so place_num should stay the same.
    } else {
      place_num = i + 1;
    }

    code += `
      <tr class="${ (p.dropped() ? "dropped" : "") }">
        <td>${place_num}</td>
        <td>${p.name}</td>
        <td class="stat">${ p.get_match_points(as_of_round) }</td>
        <td class="stat">${ p.get_displayable_omw(as_of_round) }</td>
        <td class="stat">${ p.get_displayable_gw(as_of_round) }</td>
        <td class="stat">${ p.get_displayable_ogw(as_of_round) }</td>
      </tr>
    `;

  }



  code += "</tbody></table>";

  return code;

}  // end function get_standings_table()



/**
 * Return an HTML input field of the "number" type, based on the provided data
 * object.
 *
 * Keys in the object are mostly the names of HTML attributes, but not always,
 * and you can't just send arbitrary attributes.  Read the function for
 * specifics and to see what the default attributes are.
 *
 * In particular, note:
 *
 * - The "data" key, whose value is a sub-object that specifies how to create
 *   various data-* attributes.
 * - The "span_class" key, which specifies the HTML class to set on a <span>
 *   that encloses everything that's returned.
 * - That there is no "label" key, but rather "prefix_label" and "postfix_label"
 *   keys so you can specify whether you want the label to come before or after
 *   the input (and they may be used together).
 *
 * @param  {object} cfg An object that specifies what to set for various attributes of the field.
 * @return {string}     HTML code containing the <input> element and associated elements.
 */
function make_number_field(cfg) {

  let code = '';

  let defaults = {
    data: {},
    disabled: false,
    span_class: "number_field_set",
    value: "",
  };

  // Copy defaults into cfg.
  cfg = Object.assign(defaults, cfg);

  // Escape double quotes in anything that's going to be used as an attribute.
  for (let prop in cfg) {
    if (typeof cfg[prop] == "string") {
      cfg[prop] = cfg[prop].replace(/"/, '&quot;');
    }
  }
  for (let prop in cfg.data) {
    if (typeof cfg.data[prop] == "string") {
      cfg.data[prop] = cfg.data[prop].replace(/"/, '&quot;');
    }
  }

  // Start building the HTML string.
  code += `<input type="number" value="${cfg.value}"`;

  // Add arbitrary attributes to it.
  for (let attr of ['name', 'id', 'class', 'min', 'max', 'placeholder']) {
    if ( cfg.hasOwnProperty(attr) ) { code += ` ${attr}="${cfg[attr]}"`; }
  }
  // Have to do this one separately because the JS property is named with an
  // underscore but the HTML attribute is named with a hyphen.
  if ( cfg.hasOwnProperty('aria_label') ) { code += ` aria-label="${cfg.aria_label}"`; }

  if (cfg.disabled) { code += ` disabled`; }

  // Add arbitrary pieces of data.
  for (let d in cfg.data) {
    code += ` data-${d}="${cfg.data[d]}"`;
  }

  code += `>`;

  // Add decrement & increment buttons before & after the field.
  code = `<button class="number_field_decrement" aria-label="decrease by 1" ${cfg.disabled ? 'disabled' : ''}>&minus;</button>`
       + code
       + `<button class="number_field_increment" aria-label="increase by 1" ${cfg.disabled ? 'disabled' : ''}>+</button>`;

  // The label may contain HTML code.
  if ( cfg.hasOwnProperty('prefix_label') && cfg.hasOwnProperty('id') ) {
    code = `<label for="${cfg.id}">${cfg.prefix_label} </label>` + code;
  }
  if ( cfg.hasOwnProperty('postfix_label') && cfg.hasOwnProperty('id') ) {
    code += `<label for="${cfg.id}"> ${cfg.postfix_label}</label>`;
  }

  // Wrap the whole thing in a <span> element.
  code = `<span class="${cfg.span_class}">${code}</span>`;

  return code;

}



/**
 * Specifically for opening one or more of the sections in the #players region
 * of the page.
 *
 * See also close_sections().
 *
 * @param  {string} selector A CSS selector indicating which section(s) to open.  Remember that you can use commas to close multiple sections.
 * @return {void}
 */
function open_sections(selector) {
  $(selector).slideDown();
  $(selector).prev("h2,h3").addClass("expanded");
  $(selector).prev("h2,h3").removeClass("collapsed");
}



/**
 * Pause the clock, with the intent to resume it from the same point later.
 *
 * @return {void}
 */
function pause_clock() {
  clearInterval(clock_interval);
  $("button#resume_clock").show();
  $("button#pause_clock").hide();
}



/**
 * Call this when the "Pair the unpaired players with each other" button is
 * clicked.  It makes the Round object do the new pairing, then updates the
 * display.
 *
 * @return {void}
 */
function pair_unpaired_and_update() {

  let r = tournament.get_lrn();

  // Create matches with this round's unpaired players, if possible, and
  // add them to the round.
  let success = tournament.rounds[r].pair_unpaired_players();

  // If new pairings were generated, save them and then update the page
  // display to show them.
  if (success) {

    save();

    $(".round[data-round-num="+r+"] .pairings table tbody")
      .replaceWith( tournament.rounds[r].get_pairings_tbody() );

  }

  // Now we probably need to hide the button again.
  show_or_hide_pair_unpaired_players_control();

  // Presumably the user wouldn't have clicked the button if they weren't
  // done adding the late players.  In any case, we want to draw their
  // attention to the fact that the work they requested has been done, so we
  // close the #player_list section and open the pairings section.
  close_sections("#player_list");
  open_sections(".round[data-round-num="+r+"], .round[data-round-num="+r+"] .pairings");

}



/**
 * Return a previously dropped player to the tournament.  See also
 * drop_player().
 *
 * Note that this will not affect the current round's pairings.
 *
 * @param  {number} who The PID of the player to reinstate.
 * @return {void}
 */
function reinstate_player(who) {

  who = parseInt(who);  // make sure it's an integer
  tournament.get_player_by_id(who).reinstate();

  // Update the display.
  update_player_list(tournament.players);
  update_standings_tables();

  save();

}



/**
 * Call this when the "Restore default settings" button is clicked, to change
 * all settings back to their default values.
 *
 * The include_display_settings parameter is not actually being used, but it's
 * there in case I decide to use it.
 *
 * @param {bool} include_display_settings Whether or not to reset the settings that are only cosmetic.
 *
 * @return {void}
 */
function restore_default_settings(include_display_settings = true) {

  let latest_round = 0;
  if (typeof tournament !== "undefined") {
    latest_round = tournament.rounds.length;
  }

  if (include_display_settings) {
    $("#show_points_toggle").prop("checked", tournament.default_show_points);
    apply_show_points_setting();

    $("#show_dropped_players_toggle").prop("checked", tournament.default_show_dropped_players);
    apply_show_dropped_players_setting();
  }

  $("#points_per_win").val(tournament.default_points_for_a_win);
  apply_points_per_win_setting();

  $("#points_per_draw").val(tournament.default_points_for_a_draw);
  apply_points_per_draw_setting();

  $("#first_table").val(tournament.default_first_table_num);
  apply_first_table_setting();

  // Since changing this setting may pop up a warning dialog,
  // let's not do it if it's still on its default value.
  if ( parseInt( $("#games_to_win_a_match").val() ) != tournament.default_games_to_win_a_match ) {

    // I don't think we should permit changing the number of games per match in
    // the middle of a tournament.
    if (tournament.rounds.length < 1) {
      $("#games_to_win_a_match").val(tournament.default_games_to_win_a_match);
      apply_games_to_win_setting();
    } else {
      show_dialog("games_to_win_change");
    }

  }

  // Probably redundant, but whatever.
  save();

}


/**
 * A convenient way to move from standings back to pairings.
 * We only make this available in the latest round's section.
 *
 * @return {void}
 */
function return_to_pairings() {

  close_sections(".round[data-round-num='"+tournament.get_lrn()+"'] .standings");
  open_sections(".round[data-round-num='"+tournament.get_lrn()+"'] .pairings");

  $(".round[data-round-num='"+tournament.get_lrn()+"'] .pairings input[type='number']:first").focus();

}  // end function return_to_pairings()



/**
 * Write the tournament & player info to the browser's storage.
 *
 * @return {void}
 */
function save() {
  localStorage.setItem( 'tournament', JSON.stringify(tournament) );
}



/**
 * Call this every time you want to change what the clock is showing.
 *
 * @param  {number} ms The amount of time to show on the clock, in milliseconds.
 * @return {void}
 */
function show_clock(ms) {

  // If the time is negative, remember that, and then make it positive.
  sign = (ms < 0 ? "-" : "");
  ms = Math.abs(ms);

  $("#time").text( sign + zero_pad( Math.floor(ms / 1000 / 60) ) + ":" + zero_pad( Math.ceil(ms / 1000 % 60) ) );

}



/**
 * Decides whether the "Pair the unpaired players with each other" should be
 * shown or hidden (and then does that).
 *
 * Note that even if there's just one unpaired player and nobody with a bye,
 * we do allow pairing that player (effectively, giving them the bye).
 * If there are three unpaired players and you want to pair them, it doesn't
 * make sense to put two of them in a match but then refuse to give the
 * third the bye.  So, if we'd give a bye to an unpaired player when there's
 * three of them, what justification would there be for not doing the same
 * even when there's just one?
 *
 * @return {void}
 */
function show_or_hide_pair_unpaired_players_control() {

  // If there aren't any rounds yet, then there definitely aren't any
  // "unpaired" players to pair.
  if (   typeof tournament === "undefined"
      || tournament.rounds.length < 1
  ) {
    $("#pair_unpaired_set").slideUp();
    return;
  }

  // Count the number of players, not including the bye dummy player,
  // that are in matches in this round.
  let num_real_players_in_round = _.reduce(
    tournament.rounds[ tournament.get_lrn() ].matches,
    function (memo, match) {
      return memo + _.reject(match.players, function(p){ return p.pid === 0; }).length;
    },
    0
  );

  // We don't actually need the players "in standings order", but we need to
  // exclude the bye and players who have dropped, which the
  // get_players_in_standings_order() method does by default.
  let num_undropped_players_in_tournament = tournament.get_players_in_standings_order().length;

  // If there are more players in the tournament than in the round,
  // we need to show the controls to pair more.
  if (num_undropped_players_in_tournament > num_real_players_in_round) {

    $("#pair_unpaired_set").slideDown();

  } else {

    $("#pair_unpaired_set").slideUp();

  }

}



/**
 * Start or resume the round timer.
 *
 * There are two separate buttons for that, but internally they do the same
 * thing.
 *
 * @return {void}
 */
function start_clock() {

  // If we don't know how much time is left in the current round, assume we're starting a new round.
  if ( isNaN(time_left_in_round) ) {

    // Get the length of the round from the "minutes" field.
    length_of_round = parseInt( $("#minutes").val() ) * 60 * 1000;  // need milliseconds
    time_left_in_round = length_of_round;

    // Uncomment during testing/debugging to set these values to 10 seconds.
    //length_of_round = 10000;
    //time_left_in_round = 10000;

  };

  // Show the full time in the round right away.
  show_clock(time_left_in_round);

  // Set up an interval to update the clock display once every second.
  clock_interval =
    setInterval(function(){

      // Tick down by one second.  Remember that time_left_in_round is a number of milliseconds.
      time_left_in_round -= one_second;

      // Turn the clock red if time is up.
      if ( time_left_in_round < 0 ) {
        $("#clock").removeClass("low");
        $("#clock").addClass("over");

      // Or turn it yellow if time is low (defined as less than 10% remaining).
      } else if (time_left_in_round <= length_of_round * 0.1) {
        $("#clock").addClass("low");
      }

      show_clock(time_left_in_round);

    }, one_second);

  $("button#start_clock").hide();
  $("button#resume_clock").hide();
  $("button#pause_clock").show();
  $("button#stop_clock").show();

}



/**
 * Stop the clock, and don't allow restarting from the same point.
 *
 * @return {void}
 */
function stop_clock() {
  clearInterval(clock_interval);
  time_left_in_round = NaN;  // forgetting this value will force start_clock() to re-determine it next time it runs
  $("#time").text("--:--");
  $("button#start_clock").show();
  $("button#resume_clock").hide();
  $("button#pause_clock").hide();
  $("button#stop_clock").hide();
  $("#clock").removeClass("low");
  $("#clock").removeClass("over");
}



/**
 * Rewrite the player list code.
 *
 * @param  {array} listed_players A list of Player objects.
 * @return {void}
 */
function update_player_list(listed_players = null) {

  // Establish default values.
  if ( listed_players === null ) {
    listed_players = tournament.players;
  }

  // Remember the code for the item that lets you add a new player
  let new_player_item = $("#new_player_item");

  // Clear out whatever's currently in the player list
  $("#registration_list").empty();

  // filter out the Bye player, whose player ID is 0
  listed_players = _.filter(listed_players, function(p){ return p.pid !== 0; });

  // Iterate over the filtered player list and add each player to the page.
  for (let i = 0; i < listed_players.length; i++) {
    let p = listed_players[i];

    $("#registration_list")
      .append(`
        <li class="${(p.dropped() ? "dropped" : '')}"><span class="player_name">${p.name}</span>
        <a href="#" class="remove"    data-pid="${p.pid}">(remove)</a>
        <a href="#" class="drop"      data-pid="${p.pid}">(drop)</a>
        <a href="#" class="reinstate" data-pid="${p.pid}">(reinstate)</a>
        <label class="ftn_control"><input type="checkbox" data-pid="${p.pid}"${_.isNull(p.ftn) ? '' : ' checked'}> keep at same table</label>
        </li>
      `);
  }

  // Always put the controls for adding a new player at the end of the list -- even if
  // the tournament has already started, you're always allowed to add more players.
  $("#registration_list").append(new_player_item);

}



/**
 * Go through every round and update its standings table.
 *
 * @return {void}
 */
function update_standings_tables() {

  for (let r = 0; r < tournament.rounds.length; r++) {
    $(".round[data-round-num="+r+"] .standings table")
      .replaceWith( get_standings_table(r) );
  }

}  // end function update_standings_tables()



/**
 * Run this on page load to write out the sections for every round that was
 * loaded.
 *
 * @param  {number} up_to_round [description]
 * @return {void}
 */
function write_out_round_sections(up_to_round = null) {

  // Check for errors.
  if (typeof tournament === "undefined") {
    console.error("There's no global 'tournament' variable at this time.");
    return;
  }

  // Return early if there's nothing to do.
  if (tournament.rounds.length < 1) {
    return;
  }

  // Establish defaults.
  if (up_to_round === null) {
    up_to_round = tournament.get_lrn();
  }

  // Loop over each round.
  for (let r = 0; r <= up_to_round; r++) {
    create_round_section(r);
  }  // end for-loop

}  // end function write_out_round_sections()



/**
 * Converts an (assumed positive) number to a string, adding a zero to the left
 * if it's less than two digits.
 *
 * @param  {number} n A positive number.
 * @return {string}   The same number as a string, padded with a zero if necessary.
 */
function zero_pad(n) {
  return (n < 10 ? "0" : "") + n;
}
