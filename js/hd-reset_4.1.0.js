/*
 * hd-reset.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * Makes it possible to delete all data from localStorage if something goes
 * terribly wrong and you can't even do basic things, e.g. use the "Start a
 * new tournament" button.  This is in a file by itself to minimize the
 * chances that bad code elsewhere will prevent this code from running.  This
 * should be the one thing on the site that still works when nothing else does.
 */

$(document).ready(function(){
  $("#reset").click(function(e){

    e.preventDefault();

    let message = "Clearing out your tournament records may fix whatever " +
      "problems you're having with this site, but any tournament data that " +
      "you've entered will be gone, and you won't be able to retrieve it. " +
      "Are you sure you want to clear your records?";

    if ( window.confirm(message) ) {
      localStorage.clear();
      window.alert("Records cleared!");
    }

  });
});
