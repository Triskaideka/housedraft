/**
 * hd-start.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * This file contains the code that should be run when the page loads, to
 * initialize everything.
 */

// On page load.
$(document).ready(function(){

  // Upgrade all number fields.
  // It's important to do this before loading the tournament, because we may
  // change the values in some of these fields depending on what's saved in the
  // tournament.
  $("#minutes").replaceWith(make_number_field({
    id: "minutes",
    min: 0,
    postfix_label: "minutes",
    value: 50
  }));
  $("#games_to_win_a_match").replaceWith(make_number_field({
    id: "games_to_win_a_match",
    min: 1,
    prefix_label: "Matches end after one player wins",
    postfix_label: "games",
    value: 2
  }));
  $("#points_per_win").replaceWith(make_number_field({
    id: "points_per_win",
    min: 0,
    prefix_label: "Points for a win:",
    value: 3
  }));
  $("#points_per_draw").replaceWith(make_number_field({
    id: "points_per_draw",
    min: 0,
    prefix_label: "Points for a draw:",
    value: 1
  }));
  $("#first_table").replaceWith(make_number_field({
    id: "first_table",
    prefix_label: "Starting table number:",
    value: 1
  }));



  let stored_tournament = localStorage.getItem("tournament");  // getItem should return null if no stored tournament
  let parsed_tournament = JSON.parse(stored_tournament);

  // Try to load saved tournament data if there is any.
  if (stored_tournament && parsed_tournament) {

    let tournament_is_corrupted = false
      , tournament_is_empty = true
      ;

    tournament = new Tournament(parsed_tournament);



    // Turn the list of rounds that we loaded from the serialized JSON
    // into full-fledged Round objects.
    if ( tournament.hasOwnProperty("rounds") ) {

      // Re-create each Round object in turn.
      for (let i = 0; i < tournament.rounds.length; i++) {
        tournament.rounds[i] = new Round(tournament.rounds[i]);
      }

    } else {
      tournament_is_corrupted = true;
    }

    // Turn the list of players that we loaded from the serialized JSON
    // into full-fledged Player objects.
    if (   ! tournament_is_corrupted
        && tournament.hasOwnProperty("players")
    ) {

      // Re-create each Player object in turn.
      for (let i = 0; i < tournament.players.length; i++) {
        tournament.players[i] = new Player(tournament.players[i]);
      }

      // If there are any (real) players, then we need to be able to start the
      // tournament.
      enable_or_disable_starting_controls();

    } else {
      tournament_is_corrupted = true;
    }



    // A tournament is empty if it contains no rounds and no players (other
    // than the Bye).
    // (In the normal flow of events, no players should imply no rounds,
    // but let's not make the assumption.)
    if (   ( tournament.hasOwnProperty("players") && tournament.players.length > 1 )
        || ( tournament.hasOwnProperty("rounds") && tournament.rounds.length > 0 )
    ) {
      tournament_is_empty = false;
    }



    // Is there a version mismatch/other problem?
    if (   tournament_is_corrupted
        || ! tournament.version
        || tournament.version.major !== version.major
    ) {

      // If there's no data that the user might care about in the tournament,
      // just clobber it freely.
      if (tournament_is_empty) {

        tournament = new Tournament;

      // If there *is* data in the tournament, warn the user that they
      // need to start over.
      } else {

        // Figure out what we can call the existing tournament's version.
        let existing_tournament_version = "an unknown version";
        if ( tournament.hasOwnProperty("version") ) {
          if ( tournament.version.hasOwnProperty("major") ) {
            existing_tournament_version = "version <strong>" + tournament.version.major;
            if ( tournament.version.hasOwnProperty("minor") ) {
              existing_tournament_version += "." + tournament.version.minor;
              if ( tournament.version.hasOwnProperty("bugfix") ) {
                existing_tournament_version += "." + tournament.version.bugfix;
              }
            }
            existing_tournament_version += "</strong>";
          }
        }

        show_dialog({
          "title": "Update required",
          "message": `
            The current version of this web site is <strong>${version.major}.${version.minor}.${version.bugfix}</strong>,
            but the tournament stored in your browser was created using ${existing_tournament_version}.
            It may not work correctly.  You should use the "Start a new tournament" button to start over.
            (If that doesn't work, then use the "clear out your tournament records" link on the Extras page.)
          `
        });

      }  // end if (tournament_is_empty)

    }  // end if (there was a version mismatch)



    // Continue as if there's nothing wrong with the tournament data (even if
    // there is, we'll still try to load it; maybe things will work out).

    // Settings:
    $("#games_to_win_a_match").val(tournament.games_to_win_a_match);
    $("#points_per_win").val(tournament.points_for_a_win);
    $("#points_per_draw").val(tournament.points_for_a_draw);
    $("#first_table").val(tournament.first_table_num);

    if ( tournament.show_points_in_pairings ) {
      $("#show_points_toggle").prop("checked", true);
      $("main").addClass("show_points");
    } else {
      $("#show_points_toggle").prop("checked", false);
      $("main").removeClass("show_points");
    }

    if ( tournament.show_dropped_players_in_standings ) {
      $("#show_dropped_players_toggle").prop("checked", true);
      $("main").removeClass("hide_dropped_players_from_standings");
    } else {
      $("#show_dropped_players_toggle").prop("checked", false);
      $("main").addClass("hide_dropped_players_from_standings");
    }



    // Display.
    if ( ! _.isEmpty(tournament.draft_seating) ) {
      display_draft_seating();
    }

    write_out_round_sections();

    if (tournament.rounds.length > 0) {

      $("main").addClass("underway");
      $("#delete_latest_round").prop("disabled", false);
      close_sections("#player_list");

    }

    enable_or_disable_starting_controls();
    update_player_list(tournament.players);

  // If there wasn't any tournament data to load, create a blank tournament.
  } else {

    tournament = new Tournament;

  }



  /* Set up all the buttons and other controls. */

  // Make settings expand/contract link work.
  $("#settings_toggle").click(function(e){
    $("#settings").slideToggle(400, autosize_announcements);
    $("#settings_toggle span").toggle();
    e.preventDefault();
  });

  // Make "games_to_win_a_match" setting work.
  $("#games_to_win_a_match").change(apply_games_to_win_setting);

  // Make points settings work.
  $("#points_per_win").change(apply_points_per_win_setting);
  $("#points_per_draw").change(apply_points_per_draw_setting);

  // Make "show points" checkbox work.
  $("#show_points_toggle").change(apply_show_points_setting);

  // Make "Show dropped players in standings" checkbox work.
  $("#show_dropped_players_toggle").change(apply_show_dropped_players_setting);

  // Make "Starting table number" field work.
  $("#first_table").change(apply_first_table_setting);

  // Make the "Restore default settings" button work.
  $("#restore_default_settings").click(restore_default_settings);

  // Make expand/contract links in the player column work
  // Could use open_sections() and close_sections to accomplish this, but by using the toggle functions we avoid
  // having to know whether the section is currently opened or closed.
  $(document).on('click', "#players h2, #players h3", function(e){
    $(this).toggleClass("expanded collapsed");
    $(this).next().slideToggle();
    e.preventDefault();
  });

  // Make "Start a new tournament" button work.
  $("#clear_tournament").click(function(){
    show_dialog("clear_tournament");
  });

  // Make "Add a player" button work.
  $(document).on("click", "#add_player", function(e){
    e.stopPropagation();
    add_player();
  });

  // If the user presses Enter while in the new player name field, assume they
  // were done typing the player's name.
  //
  // keydown or keypress seem okay for this, but keyup causes problems wherein
  // this event is immediately re-run if you press Enter to dismiss the dialog
  // box that warns you about a duplicate player name.
  $(document).on("keypress", "#new_player_name", function(e){
    if (e.key == "Enter") {
      e.preventDefault();
      $("#add_player").click();
    }
  });

  // Make the links to remove players work.
  $("#players").on("click", ".remove", function(e){
    deregister_player(
      parseInt(
        $(this).data("pid")
      )
    );
    e.preventDefault();
    $("input#new_player_name").focus();
  });

  // Make the links to drop players work.
  $("#players").on("click", ".drop", function(e){
    drop_player(
      parseInt(
        $(this).data("pid")
      )
    );
    e.preventDefault();
  });

  // Make the links to reinstate dropped players work.
  $("#players").on("click", ".reinstate", function(e){
    reinstate_player(
      parseInt(
        $(this).data("pid")
      )
    );
    e.preventDefault();
  });

  // Make the fixed table number control work.
  $("#players").on("click", ".ftn_control input", function(e){
    tournament.get_player_by_id(
      parseInt(
        $(this).data("pid")
      )
    ).set_ftn(
      $(this).prop("checked")
    );
    save();
  });

  // Changing the number of games in a match shouldn't be allowed after round 1.
  enable_or_disable_games_to_win_control();

  // Determine whether or not to show the "Pair the unpaired players with each
  // other" button.
  show_or_hide_pair_unpaired_players_control();

  // Make the "Pair the unpaired players with each other" button work.
  $("#pair_unpaired_players").click(function(){
    confirm_pair_unpaired();
  });

  // Make "assign seating" button work.
  $("#assign_seating").click(function(){
    confirm_assign_seating();
  });

  // Make "Delete latest round" button work.
  $("#delete_latest_round").click(function(){
    confirm_delete_latest_round();
  });

  // Make "Done entering results for round N" buttons work.
  // There's a button like this in every round's pairings section.
  $(document).on('click', "button.advance_to_standings", function(){
    confirm_advance_to_standings();
  });

  // Make "Return to pairings" buttons work.
  // There's a button like this in every round's standings section.
  $(document).on('click', "button.return_to_pairings", function(){
    return_to_pairings();
  });

  // Make "Advance to round N" buttons work.
  // There's a button like this in the player list, plus one in the section for
  // every round.
  $(document).on('click', "button.advance_to_next_round", function(){
    confirm_create_new_round();
  });

  // Make the buttons that decrement number fields work.
  $(document).on("click", "button.number_field_decrement", function(){

    let adjacent_number_field = $(this).next("input[type=number]");
    let v = Number(adjacent_number_field.val());

    // Don't decrement below the minimum value for the field, if one was set.
    if (   typeof adjacent_number_field.attr("min") == "undefined"
        || v - 1 >= Number(adjacent_number_field.attr("min"))
    ) {
      adjacent_number_field.val(v - 1);
      adjacent_number_field.trigger("change");
    }

  });

  // Make the buttons that increment number fields work.
  $(document).on("click", "button.number_field_increment", function(){

    let adjacent_number_field = $(this).prev("input[type=number]");
    let v = Number(adjacent_number_field.val());

    // Don't increment beyond the maximum value for the field, if one was set.
    if (   typeof adjacent_number_field.attr("max") == "undefined"
        || v + 1 <= Number(adjacent_number_field.attr("max"))
    ) {
      adjacent_number_field.val(v + 1);
      adjacent_number_field.trigger("change");
    }

  });

  // Save match results as soon as one is entered.
  $(document).on('change', ".pairings table input[type=number]", function(){
    collect_results();
  });

  // Make clock buttons work.
  $("#start_clock, #resume_clock").click(function(){
    start_clock();
  });
  $("#pause_clock").click(function(){
    pause_clock();
  });
  $("#stop_clock").click(function(){
    stop_clock();
  });

  // Set up some rotating announcements.
  $("#message textarea").text( _.sample(random_announcements) );

  announcement_rotation_interval =
    setInterval(function(){

      // If the annnouncements area is empty, or contains one of the pre-set announcements,
      // replace it with another pre-set announcement.
      if (   $("#message textarea").text() === ""
          || _.contains( random_announcements, $("#message textarea").text() )
      ) {
        $("#message textarea").text( _.sample(random_announcements) );
      }

    }, 60000);  // one minute

  // Make the announcements box fill the available space.
  autosize_announcements();
  $(window).resize( autosize_announcements );

  // Vertically center the dialog box on resize.
  $(window).resize(function(){
    $("#dialog_box").css("top", (( $(window).height() - $("#dialog_box").height() ) / 2) );
  });

});
