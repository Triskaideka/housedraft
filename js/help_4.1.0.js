/**
 * help.js
 *
 * Part of HouseDraft:  https://housedraft.games/
 *
 * This file contains automation code for the Instructions and About pages.
 */

// On page load
$(document).ready(function(){

  // Use expand/collapse links to streamline the main set of instructions.
  $("#main_instr ul").before(`
    <a class="notes_link more_notes" href="#">More &#187;</a>
    <a class="notes_link hide_notes hidden" href="#">Hide notes &#171;</a>
  `).hide();

  // Make the expand links work.
  $("a.more_notes").click(function(e){
    $(this).hide();
    $(this).next().show();
    $(this).next().next().slideDown();
    e.preventDefault();
  });
  $("a.more_notes_all").click(function(e){
    $("a.more_notes").hide();
    $("a.hide_notes").show();
    $("#main_instr ul").slideDown();
    e.preventDefault();
  });

  // Make the collapse links work.
  $("a.hide_notes").click(function(e){
    $(this).prev().show();
    $(this).hide();
    $(this).next().slideUp();
    e.preventDefault();
  });
  $("a.hide_notes_all").click(function(e){
    $("a.more_notes").show();
    $("a.hide_notes").hide();
    $("#main_instr ul").slideUp();
    e.preventDefault();
  });



  // Iterate over subheadings in order to assemble tables of contents for them.
  $("#extraordinary h4, #questions dt").each(function(index){

    let link_code = `<li>`;

    // If I forgot to give this heading a named index, make up a generic one
    // so it's still linkable.
    if ( ! $(this).attr('id') ) {
      $(this).attr('id', 'item_' + (index+1));
    }

    // Link to this heading.
    link_code += `<a href="#${ $(this).attr('id') }">${ $(this).text() }</a></li>`;

    $(".toc").append(link_code);

  });

});
