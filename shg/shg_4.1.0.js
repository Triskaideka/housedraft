/*
shg.js
Sample Hand Generator for MtG and other card games.
https://housedraft.games/shg/
*/

var deck = []  // all of the cards that are in the deck when the game starts
  , library = []  // the cards remaining in the deck as the game goes on
  , starting_hand_size = 0  // need to keep track of it globally for the sake of the mulligan feature
  ;



// draw n cards and display them
function draw (n) {

  if (n > 0 && library.length > 0) {
    while (n--) {

      name = library.shift();

      classes = '';
      if (name.toLowerCase() == 'plains')   { classes = 'white land'; }
      if (name.toLowerCase() == 'island')   { classes = 'blue  land'; }
      if (name.toLowerCase() == 'swamp')    { classes = 'black land'; }
      if (name.toLowerCase() == 'mountain') { classes = 'red   land'; }
      if (name.toLowerCase() == 'forest')   { classes = 'green land'; }

      $('#hand').html(
        $('#hand').html() + '<div class="card ' + classes + '">' + name + '</div>'
      );

    }
  }

  // update the count of cards left in the library
  $('#lib_count').text(library.length);

} // end function draw()



// Perform a Fischer-Yates shuffle on an array and return it.
// from https://stackoverflow.com/a/6274398
function shuffle (array) {

  array = array.slice(0);  // clone it first

  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {

    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;

  }

  return array;

}



// "Clear deck" button
$('#clear').click(function(){
  $('textarea').val('');
});



// "Shuffle and deal" Button
$('#shuffle').click(function(){

  var decklist = $('textarea').val().split("\n")
    , i = 0
    ;

  starting_hand_size = ($('#shs').val() || 7);



  // clear out anything that's already been dealt
  $('#hand').html('');



  // make the deck
  deck = [];

  for (i = 0; i < decklist.length; i++) {

    var line = decklist[i]
      , deck_line_regex = /^([0-9]+)\s?[xX]?\s(.*)$/
      ;


    // don't shuffle the sideboard into the deck
    if ( line.match(/^Sideboard/i) ) { break; }


    // parse the line *if* it appears to be a card
    if ( line.match(deck_line_regex) ) {

      // 'x' is discarded
      var [x, count, name] = line.match(deck_line_regex);

      count = parseInt(count);

      while (count--) {
        deck.push(name);
      }

    }

  }

  library = shuffle(deck);



  // Can't draw more cards than there are in the deck.
  if (starting_hand_size > library.length) {
    starting_hand_size = library.length;
  }



  draw(starting_hand_size);



  // now the mulligan and draw buttons can be used
  $('#mulligan').prop("disabled", false);
  $('#draw').prop("disabled", false);

});



// "Mulligan" button
$('#mulligan').click(function(){

  if (starting_hand_size > 0) { starting_hand_size--; }

  // turn off the mulligan button if there's no more mulliganning to be done
  if (starting_hand_size <= 0) { $('#mulligan').prop("disabled", true); }

  $('#hand').html('');
  library = shuffle(deck);
  draw(starting_hand_size);

});



// "Draw one" button
$('#draw').click(function(){
  draw(1);
});



// Click a card to dismiss it
$(document).on('click', '.card', function(evt){
  evt.stopPropagation();
  $(this).detach();
});
